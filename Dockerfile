FROM debian:buster-slim

RUN mkdir /var/opt/webdns; chown daemon /var/opt/webdns; apt-get update && apt-get install -y \
 curl \
 bind9 \
 libarchive-zip-perl \
 libcatalyst-controller-actionrole-perl \
 libcatalyst-engine-apache-perl \
 libcatalyst-modules-perl \
 libcatalyst-perl \
 libcatalyst-plugin-log-dispatch-perl \
 libcatalyst-plugin-unicode-encoding-perl \
 libcatalyst-view-json-perl \
 libcatalyst-view-tt-perl \
 libcrypt-blowfish-perl \
 libcrypt-cbc-perl \
 libdata-password-perl \
 libdata-validate-domain-perl \
 libdata-validate-email-perl \
 libdata-validate-ip-perl \
 libdata-validate-perl \
 libdata-validate-struct-perl \
 libdata-validate-uri-perl \
 libdate-manip-perl \
 libdate-simple-perl \
 libdatetime-format-http-perl \
 libdatetime-format-pg-perl \
 libdatetime-set-perl \
 libdbd-pg-perl \
 libdbix-class-perl \
 libdigest-hmac-perl \
 libdigest-sha-perl \
 libfcgi-procmanager-perl \
 libfile-slurp-perl \
 libhtml-formfu-model-dbic-perl \
 libhtml-formfu-perl \
 libidna-punycode-perl \
 libio-socket-ip-perl \
 libjson-perl \
 liblist-compare-perl \
 libmath-round-perl \
 libmime-base64-perl \
 libmoose-perl \
 libnet-dns-perl \
 libnet-ip-perl \
 libnet-openssh-perl \
 libnet-sftp-foreign-perl \
 libnet-whois-parser-perl \
 libtext-csv-perl \
 libyaml-perl \
 libyaml-syck-perl

ADD . /opt/webdns

USER daemon
EXPOSE 3000
WORKDIR /opt/webdns
ENV PERL5LIB=/opt/webdns/lib CATALYST_CONFIG=/etc/webdns.yaml CATALYST_HOME=/opt/webdns CATALYST_DEBUG=1

ENTRYPOINT ["/opt/webdns/entrypoint.sh"]
