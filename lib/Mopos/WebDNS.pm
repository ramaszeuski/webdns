package Mopos::WebDNS;
use Moose;
use namespace::autoclean;

use Catalyst::Runtime 5.80;

# Set flags and add plugins for the application.
#
# Note that ORDERING IS IMPORTANT here as plugins are initialized in order,
# therefore you almost certainly want to keep ConfigLoader at the head of the
# list if you're using it.
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

use Catalyst qw/
    ConfigLoader
    Static::Simple
    Authentication
    Authorization::Roles

    Navigation

    Session
    Session::Store::File
    Session::State::Cookie

    Log::Dispatch
/;

extends 'Catalyst';

our $VERSION = '0.01';

# Configure the application.
#
# Note that settings in mopos_webdns.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config(
    name     => 'Mopos::WebDNS',
    encoding => 'utf8',

    disable_component_resolution_regex_fallback => 1,
    enable_catalyst_header => 1,

    authentication => {
      default_realm => 'users',
      realms        => {
         users => {
            credential => {
               class          => 'Password',
               password_field => 'password',
               password_type  => 'clear'
            },
            store => {
               class         => 'DBIx::Class',
               user_model    => 'DB::User',
               id_field      => 'login',
               role_column   => 'roles',
            }
         },
      },
   },

    'Controller::HTML::FormFu' => {
        constructor => {
            render_method => 'tt',
            tt_args       => {
                ENCODING => 'utf-8',
            },
        },
        model_stash => {
            schema => 'DB',
        },
    },

   'Plugin::Static::Simple' => {
        include_path => [
            'root/static',
        ],
    },

    'Plugin::Session' => {
        cookie_expires => 0,
        cookie_secure  => 0,
        storage        => '/tmp/session_webdns',
    },
);

# Start the application
__PACKAGE__->setup();


=head1 NAME

Mopos::WebDNS - Catalyst based application

=head1 SYNOPSIS

    script/mopos_webdns_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<Mopos::WebDNS::Controller::Root>, L<Catalyst>

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
