package Mopos::WebDNS::View::HTML;

use strict;
use warnings;

use base 'Catalyst::View::TT';

__PACKAGE__->config(
    render_die          => 1,
    ENCODING            => 'utf-8',
    TEMPLATE_EXTENSION  => '.tt2',
    WRAPPER             => 'wrapper.tt2',
    INCLUDE_PATH        => [
        'root/src',
        'root/lib',
        'root/site',
    ],
    FILTERS => {
        hms      => \&hms,
        rr_value => \&rr_value,
    }
);

sub hms {
    my $sec  = shift;

    my $hour = int($sec / 3600);
    $sec    -= $hour * 3600;

    my $min  = int($sec / 60);
    $sec    -= $min * 60;

    my $hms = sprintf('%02d:%02d:%02d', $hour, $min, $sec);
    $hms =~ s/^00://;
    $hms =~ s/^0//;

    return $hms;
}

sub rr_value {
    my $text  = shift;

#    $text =~ s/^"//;
#    $text =~ s/"$//;
    $text =~ s/\.$//;

    return $text;
}

=head1 NAME

Mopos::WebDNS::View::HTML - TT View for Mopos::WebDNS

=head1 DESCRIPTION

TT View for Mopos::WebDNS.

=head1 SEE ALSO

L<Mopos::WebDNS>

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
