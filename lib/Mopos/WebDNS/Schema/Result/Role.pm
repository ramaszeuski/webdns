package Mopos::WebDNS::Schema::Result::Role;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('role');

__PACKAGE__->add_columns(
    id => {
        is_auto_increment => 1,
        sequence          => 'uid_seq',
        data_type         => 'integer',
        is_nullable       => 0,
    },
    role => {
        data_type     => 'varchar',
        is_nullable   => 0,
        size          => 32,
    },
    active => {
        data_type     => 'bool',
        is_nullable   => 0,
        default_value => 'true',
    },
    priority => {
        data_type     => 'integer',
        is_nullable   => 0,
        default_value => 0,
    },
    name => {
        data_type     => 'text',
        is_nullable   => 0,
    },
);

__PACKAGE__->set_primary_key('id');


__PACKAGE__->has_many(
    user_roles => 'Mopos::WebDNS::Schema::Result::UserRole',
    {
        'foreign.role_id' => 'self.id'
    },
);

__PACKAGE__->many_to_many(
    users => 'user_roles', 'user'
);

1;

__END__


