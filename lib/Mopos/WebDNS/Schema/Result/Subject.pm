package Mopos::WebDNS::Schema::Result::Subject;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('subject');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        active
        created
        deleted
        login
        password
        name
        firstname
        lastname
        email
        messenger
        phone
        cellular
        fax
        country
        region
        city
        address
        zip
        profile
        notes
    ),
);

1;
