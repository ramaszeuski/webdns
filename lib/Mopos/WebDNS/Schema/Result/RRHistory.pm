package Mopos::WebDNS::Schema::Result::RRHistory;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use JSON;
use Net::DNS;

our $VERSION = 1;

__PACKAGE__->table('rr_history');

__PACKAGE__->add_columns(
    id => {
        is_auto_increment => 1,
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        rr_id
        log_id
        changed

        ttl

        preference
        weight
        port
        address
        value

    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    rr => 'Mopos::WebDNS::Schema::Result::RR',
    {
        'foreign.id' => 'self.rr_id'
    },
);

1;

__END__


