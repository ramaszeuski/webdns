package Mopos::WebDNS::Schema::Result::User;

use strict;
use warnings;

use base 'Mopos::WebDNS::Schema::Result::Subject';

our $VERSION = 2;

__PACKAGE__->table('user');

__PACKAGE__->add_columns(
    qw(
        customer_id
        roles
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    log_events => 'Mopos::WebDNS::Schema::Result::Log',
    {
        'foreign.user_id' => 'self.id'
    },
);

__PACKAGE__->belongs_to(
    customer => 'Mopos::WebDNS::Schema::Result::Customer',
    {
        'foreign.id' => 'self.customer_id'
    },
);

sub role_names {
    my $self = shift;
    my @roles = ();
    my $resultset = $self->result_source->schema->resultset('User');


    ROLE:
    foreach my $role ( split /\W/, $self->roles ) {
        push @roles, $resultset->role_name( $role );
    }

    return join ', ', @roles;

}

sub name {
    my $self = shift;
    my $name =
        join ' ',
        grep /\w/,
        (
            $self->firstname // '',
            $self->lastname // '',
        );

    return $name || $self->login;

}

sub is_deletable {
    my $self = shift;
    return 1;
}

1;

__END__
