package Mopos::WebDNS::Schema::Result::Log_view;

use strict;
use warnings;

use base 'Mopos::WebDNS::Schema::Result::Log';

our $VERSION = 1;

__PACKAGE__->table('log_view');

__PACKAGE__->add_columns(
    qw(
        user_login
        user_firstname
        user_lastname
        customer_id
        customer_name
    ),
);

sub user_fullname {
    my $self = shift;

    if ($self->user_firstname || $self->user_lastname) {
        return $self->user_firstname . ' ' . $self->user_lastname;
    }
    else {
        return $self->user_login;
    }
}

sub event_name {
    my $self = shift;
    return $self->resultset_class->event_name($self->event);
}

1;
