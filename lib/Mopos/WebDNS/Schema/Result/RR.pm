package Mopos::WebDNS::Schema::Result::RR;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use JSON;
use Net::DNS;
use Text::ParseWords;

our $VERSION = 1;

use constant DOT => q{.};

__PACKAGE__->table('rr');

__PACKAGE__->add_columns(
    id => {
        is_auto_increment => 1,
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        active
        state
        changed
        deleted
        zone_id
        customer_id

        name
        type
        class
        ttl

        preference
        weight
        port
        address
        value
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    zone => 'Mopos::WebDNS::Schema::Result::Zone',
    {
        'foreign.id' => 'self.zone_id'
    },
);

__PACKAGE__->belongs_to(
    customer => 'Mopos::WebDNS::Schema::Result::Zone',
    {
        'foreign.id' => 'self.customer_id'
    },
);

__PACKAGE__->has_many(
    history => 'Mopos::WebDNS::Schema::Result::RRHistory',
    {
        'foreign.rr_id' => 'self.id',
    },
);

__PACKAGE__->has_many(
    history_view => 'Mopos::WebDNS::Schema::Result::RRHistory_view',
    {
        'foreign.rr_id' => 'self.id',
    },
);

sub name_idn {
    my $self = shift;
    $self->result_source->schema->idn_encode( $self->name );
};

sub string {
    my ( $self, $args ) = @_;

    my $schema =$self->result_source->schema;

    my @fields = (
        $self->name_idn . DOT,

        $self->ttl,
        $self->class,
        $self->type
    );

    if ( $self->type =~ /^(CNAME|NS|PTR)$/ ) {
        push @fields, $schema->idn_encode( $self->value ) . DOT;
    }
    elsif ( $self->type eq 'MX' ) {
        push @fields, $self->preference;
        push @fields, $schema->idn_encode( $self->value ) . DOT;
    }
    elsif ( $self->type eq 'SRV' ) {
        push @fields, $self->preference;
        push @fields, $self->weight;
        push @fields, $self->port;
        push @fields, $schema->idn_encode( $self->value ) . DOT;
    }
    elsif ( $self->type eq 'TXT') {
        my $value = $self->value;

        $value =~ s/\x0d\x0a/ /gs;
        $value =~ s/\x0d/ /gs;
        $value =~ s/\x0a/ /gs;

        if ( $args->{test_smart_formattting} ) {
            my $value = join '', parse_line('\s+', 0, $value);
        }
        else {
            $value =~ s/"/\"/g;
        }

        my $rr = Net::DNS::RR->new(
            name    => $self->name,
            type    => 'TXT',
            txtdata => [ unpack("(a64)*", $value) ],
        );
        $value = $rr->string;
        $value =~ s/.+IN\s+TXT\s+//;
        push @fields, $value;
    }
    else {
        push @fields, $self->value;
    }

    return join "\t", @fields;

}

1;

__END__
