package Mopos::WebDNS::Schema::Result::Zone_view;

use strict;
use warnings;

use base 'Mopos::WebDNS::Schema::Result::Zone';

our $VERSION = 1;

__PACKAGE__->table('zone_view');

__PACKAGE__->add_columns(
    qw(
        customer_name
        network_text
        name_reverse
    ),
);

1;
