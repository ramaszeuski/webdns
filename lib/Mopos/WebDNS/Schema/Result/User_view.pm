package Mopos::WebDNS::Schema::Result::User_view;

use strict;
use warnings;

use base 'Mopos::WebDNS::Schema::Result::User';

our $VERSION = 1;

__PACKAGE__->table('user_view');

__PACKAGE__->add_columns(
    qw(
        customer_name
    ),
);

1;

