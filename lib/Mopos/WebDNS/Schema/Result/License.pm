package Mopos::WebDNS::Schema::Result::License;

use strict;
use warnings;

use Moose;
use namespace::autoclean;
extends 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('license');

__PACKAGE__->add_columns(
    id => {
        data_type    => 'integer',
        is_nullable  => 0,
        sequence     => 'uid_seq'
    },
    qw(
        customer_id
        feature_id
        granted
        used
    ),
);

__PACKAGE__->belongs_to(
    customer => 'Mopos::WebDNS::Schema::Result::Customer',
    {
        'foreign.id' => 'self.customer_id',
    },
);

__PACKAGE__->set_primary_key('id');

sub free {
    my $self = shift;
    return $self->granted - $self->used;
}

1;

