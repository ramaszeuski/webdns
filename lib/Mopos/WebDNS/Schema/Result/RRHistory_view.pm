package Mopos::WebDNS::Schema::Result::RRHistory_view;

use strict;
use warnings;

use base 'Mopos::WebDNS::Schema::Result::RRHistory';

our $VERSION = 1;

__PACKAGE__->table('rr_history_view');

__PACKAGE__->add_columns(
    qw(
        ip
        user_login
        user_firstname
        user_lastname
    ),
);

sub user_fullname {
    my $self = shift;

    if ($self->user_firstname || $self->user_lastname) {
        return $self->user_firstname . ' ' . $self->user_lastname;
    }
    else {
        return $self->user_login;
    }
}

1;

