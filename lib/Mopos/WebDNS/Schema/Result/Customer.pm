package Mopos::WebDNS::Schema::Result::Customer;

use strict;
use warnings;

use base 'Mopos::WebDNS::Schema::Result::Subject';

use constant ROOT_ID => 0;

our $VERSION = 1;

__PACKAGE__->table('customer');

__PACKAGE__->add_columns(
    qw(
        ic
        dic
        sync_key
        bank_account
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    users => 'Mopos::WebDNS::Schema::Result::User',
    {
        'foreign.customer_id' => 'self.id',
    },
);

__PACKAGE__->has_many(
    licenses => 'Mopos::WebDNS::Schema::Result::License',
    {
        'foreign.customer_id' => 'self.id',
    },
);

#__PACKAGE__->has_many(
#    zones => 'Mopos::WebDNS::Schema::Result::Zone',
#    {
#        'foreign.customer_id' => 'self.id',
#    },
#);

sub licenses_usage {
    my $self = shift;

    my $licenses;

    LICENSE:
    foreach my $license ( $self->licenses() ) {
        $licenses->{ $license->feature_id } = {
            granted   => $license->granted,
            used      => $license->used,
            free      => $license->free,
        }
    }
    return $licenses;

}

sub license_use {
    my $self    = shift;
    my $feature = shift // return;
    my $count   = shift // 1;
    my $license = $self->licenses(
        { feature_id => $feature }
    )->first // return;

    $license->update(
        {
            used => ( $license->used + $count )
        }
    );

}

sub license_release {
    my $self    = shift;
    my $feature = shift // return;
    my $count   = shift // 1;

    $self->license_use(
        $feature,
        0 - $count,
    );
}

sub kill {
    my $self = shift;
    my $args = shift;

    my $schema = $self->result_source->schema;
    my $scope_guard = $schema->txn_scope_guard;

#    ZONE:
#    foreach my $zone ( $self->zones() ) {
#        $zone->kill();
#    }

    # delete
    my $delete = { deleted => \'now()' };
    $self        ->update( $delete );
    $self->users ->update( $delete );

    # zaznam do logu
    my $log = {
        event   => 'customer_delete',
        data    => $self->id,
    };

    if ( $args->{context} ) {
        $log->{user_id} = $args->{context}->user->id;
        $log->{address} = $args->{context}->req->address;
    }
    $schema->resultset('Log')->create( $log );

    $scope_guard->commit;
}

1;

__END__


