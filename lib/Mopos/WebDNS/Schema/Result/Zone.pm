package Mopos::WebDNS::Schema::Result::Zone;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use JSON;
use Net::DNS;
use Date::Manip;
use Net::Whois::Parser;
use Net::IP;

our $VERSION = 1;

__PACKAGE__->table('zone');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        active
        state
        changed
        deleted
        expire
        customer_id
        type
        name
        options
        notes
        dnssec
        masters
        allow_transfer
        max_rr
        network
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    customer => 'Mopos::WebDNS::Schema::Result::Customer',
    {
        'foreign.id' => 'self.customer_id',
    },
);

__PACKAGE__->has_many(
    rrs => 'Mopos::WebDNS::Schema::Result::RR',
    {
        'foreign.zone_id' => 'self.id',
    },
);

__PACKAGE__->has_many(
    rrs_view => 'Mopos::WebDNS::Schema::Result::RR_view',
    {
        'foreign.zone_id' => 'self.id',
    },
);

__PACKAGE__->might_have(
    whois => 'Mopos::WebDNS::Schema::Result::Whois',
    {
        'foreign.zone_id' => 'self.id',
    },
);

__PACKAGE__->inflate_column('options', {
    inflate => sub { from_json(shift) },
    deflate => sub { to_json(shift) },
});


sub name_idn {
    my $self = shift;
    $self->result_source->schema->idn_encode( $self->name );
};

sub local_parent {
    my $self = shift;

    my $parent;
    my $top_ns_rr = $self->result_source->schema->resultset('RR')->search(
        {
            type    => 'NS',
            active  => 't',
            deleted => undef,
            name    => $self->name,
            zone_id => { '<>' => $self->id },
        }
    );

    if ( $top_ns_rr->count() ) {
        return $top_ns_rr->first->zone;
    }
    else {
        return undef;
    }
}

sub parent_name {
    my $self = shift;

    my $parent_name = $self->name;
    $parent_name =~ s/.+?\.//;
    return $parent_name;
}

sub tld_name {
    my $self = shift;

    my $tld_name = $self->name;
    $tld_name =~ /([^.]+)$/;
    return $1;
}

sub prefix_ipv6 {
    my $self = shift;

    my $ip = Net::IP->new( $self->network );

    my $prefix = $ip->ip;
    my $last   = $ip->last_ip;

    while ( $prefix ne $last ) {
        chop($prefix);
        chop($last);
    }

    return $prefix;
}

sub kill {
    my $self = shift;
    my $args = shift;

    my $schema = $self->result_source->schema;
    my $scope_guard = $schema->txn_scope_guard;

    # delete
    my $delete = { deleted => \'now()' };
    $self->update( $delete );

    # zaznam do logu
    my $log = {
        event   => 'zone_delete',
        data    => $self->id,
    };

    if ( $args->{context} ) {
        $log->{user_id} = $args->{context}->user->id;
        $log->{address} = $args->{context}->req->address;
    }
    $schema->resultset('Log')->create( $log );

    $scope_guard->commit;
}

sub string {
    my $self = shift;

    my $rrs = $self->rrs_view(
        {
            deleted => undef
        },
        {
            order_by => [
                { -asc  => 'name_reverse' },
                { -desc => 'type_priority' },
                'address',
            ]
        }
    );

    return join "\n",  map { $_->string } $rrs->all;
}

sub update_serial {
    my $self   = shift;
    my $serial = shift;

    my $soa = $self->rrs(
        {
            type    => 'SOA',
            deleted => undef,
        }
    )->first // return;

    my $soa_rr = $self->result_source->schema->resultset('RR')->construct(
        { rr => $soa }
    );

    $self->update(
        {
            changed => \'now()',
        }
    );

    my $timestamp = UnixDate( ParseDate( 'now' ), '%Y%m%d00' );
#   my $timestamp = UnixDate( ParseDate( 'now' ), '%s' );
    if ( ! $serial || $serial < $timestamp ) {
        $serial = $timestamp;
    }

    if ( $serial <= $soa_rr->serial() ) {
        $serial = $soa_rr->serial() + 1;
    }

    $soa_rr->serial( $serial );

    my $value = $soa_rr->string;
    $value =~ s/.+IN\s+SOA\s+//is;

    $soa->update(
        {
            changed => $self->changed,
            value   => $value,
        }
    );

}

sub update_secondary {
    my $self = shift;
    my $args = shift;

    my $c = $args->{context};

    return if $self->type != 2;
    return if $self->state == 1;

    my @nameservers = split ';', $self->masters;

    if ( $self->active ) {
        @nameservers = (
            @nameservers,
            map { $_->{address} } @{ $c->config->{nameservers} },
        );
    }

    my $resolver  = Net::DNS::Resolver->new(
        nameservers => \@nameservers,
    );

    # porovname serial na master serveru a lokalni
    my $soa = $self->rrs( {
        deleted => undef,
        type    => 'SOA',
    } )->first;

    if ( $soa ) {
        my $soa_local = $self->result_source->schema->resultset('RR')->construct(
            { rr => $soa }
        );
        if ( $soa_local ) {
            my $response = $resolver->search( $self->name, 'SOA' );
            if ( $response ) {
                my $soa_master = ($response->answer)[0];
                return if $soa_master && $soa_master->serial <= $soa_local->serial;
            }
        }
    }

    $self->update( { state => 1 } );

    my @rrs = ();
    @rrs = $resolver->axfr( $self->name );

    if ( scalar @rrs ) {

            $self->rrs->delete();

            RR:
            foreach my $rr ( @rrs ) {
                my $rr_data = $self->result_source->schema->resultset('RR')
                              ->parse( $rr->string );

                if ( ! $rr_data || $rr_data->{error} ) {
                    $c && $c->log->error( $rr_data->{error} );
                    next RR;
                }

                $self->add_to_rrs(
                    {
                        name => $rr->name,
                        %{ $rr_data },
                    }
                );
            }

    }

    $self->update( { state => 0 } );

}

sub get_secondary {
    my $self = shift;

    return if $self->type != 2;

    my $resolver  = Net::DNS::Resolver->new(
        nameservers => [ split ';', $self->masters ]
    );

    return $resolver->axfr( $self->name );


}

sub whois_parsed {
    my $self = shift;

    return {} if ! $self->whois;
    return {} if ! $self->whois->data;

    return parse_whois( raw => $self->whois->data, domain => $self->name_idn );

}

1;

__END__
