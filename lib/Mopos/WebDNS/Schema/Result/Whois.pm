package Mopos::WebDNS::Schema::Result::Whois;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('whois');

__PACKAGE__->add_columns(
    id => {
        is_auto_increment => 1,
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        updated
        zone_id
        data
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    zone => 'Mopos::WebDNS::Schema::Result::Zone',
    {
        'foreign.id' => 'self.zone_id'
    },
);

#sub parsed {
#}

#sub expire {
#}

1;
__END__
