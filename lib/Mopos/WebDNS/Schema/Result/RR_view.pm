package Mopos::WebDNS::Schema::Result::RR_view;

use strict;
use warnings;

use base 'Mopos::WebDNS::Schema::Result::RR';

our $VERSION = 1;

__PACKAGE__->table('rr_view');

__PACKAGE__->add_columns(
    qw(
        type_priority
        customer_name
        zone_type
        zone_deleted
        address_text
        name_reverse
    ),
);

__PACKAGE__->belongs_to(
    rr => 'Mopos::WebDNS::Schema::Result::RR',
    {
        'foreign.id' => 'self.id',
    },
);

1;

__END__
