package Mopos::WebDNS::Schema::ResultSet::License;

use strict;
use warnings;
use utf8;

use base 'DBIx::Class::ResultSet';

our $VERSION = 1;

use constant FEATURE => {
    users => { type => 0, name => 'Uživatelské účty' },
    zones => { type => 0, name => 'Domény' },
};

sub features {
    return [
        map {
            {
                id    => $_,
                name  => FEATURE->{$_}{name},
                type  => FEATURE->{$_}{type},
            }
        } ( sort keys %{ FEATURE() } )
    ];
}

sub feature {
    my $self       = shift;
    my $feature_id = shift;
    return FEATURE->{$feature_id};
}

sub feature_name {
    my $self       = shift;
    my $feature_id = shift;
    return FEATURE->{$feature_id}{name};
}

1;
