package Mopos::WebDNS::Schema::ResultSet::RR;

use strict;
use warnings;
use utf8;

use base 'DBIx::Class::ResultSet';

use Net::DNS;
use Net::IP;
use Data::Validate::IP qw(is_ipv4 is_ipv6);
use Data::Validate::Domain qw(is_domain is_domain_label);
use YAML;

use constant MAX_INT => 65535;

use constant DEFAULT_TYPE  => 'A';
use constant DEFAULT_CLASS => 'IN';
use constant DEFAULT_TTL   => 86400;

use constant TTL_MIN => 300;
use constant TTL_MAX => 86400;
use constant PREFERENCE_MIN => 0;

use constant MAX_TLSA_SIZE => 16384;

use constant ERROR_INVALID_NAME => 'Neplatné jméno záznamu';
use constant ERROR_TTL_INVALID  => 'Neplatná hodnota TTL';
use constant ERROR_TTL_OVERRIDE => 'TTL musí byt v rozmězí 300 a 86400 s';
use constant ERROR_PREFERENCE_INVALID  => 'Neplatná priorita';
use constant ERROR_PREFERENCE_OVERRIDE => 'Priorita musí byt v rozmězí 0 a 65535';

use constant ERROR_INVALID_IPV4   => 'Neplatná IP adresa';
use constant ERROR_INVALID_IPV6   => 'Neplatná IPv6 adresa';
use constant ERROR_INVALID_CNAME  => 'Neplatná hodnota CNAME';
use constant ERROR_INVALID_DOMAIN => 'Neplatné doménové jméno';
use constant ERROR_INVALID_MX     => 'Neplatná hodnota MX';
use constant ERROR_INVALID_NS     => 'Neplatná hodnota NS';
use constant ERROR_INVALID_SRV_PORT   => 'Neplatný port SRV';
use constant ERROR_INVALID_SRV_WEIGHT => 'Neplatná váha SRV';
use constant ERROR_INVALID_SRV_TARGET => 'Neplatný cíl SRV';

use constant ERROR_INVALID_TLSA_USAGE        => 'Neplatný CertificateUsage';
use constant ERROR_INVALID_TLSA_SELECTOR     => 'Neplatný Selector';
use constant ERROR_INVALID_TLSA_MATCHINGTYPE => 'Neplatný MatchingType';
use constant ERROR_INVALID_TLSA_SIZE         => 'Přiliš dlouhá hodnota';

use constant ERROR_BAD_DIGEST         => 'Neplatný digest';

use constant TTL => {
    300   => '5 minut',
    900   => '15 minut',
    1800  => '30 minut',
    3600  => '1 hodina',
    7200  => '2 hodiny',
    14400 => '4 hodiny',
    28800 => '8 hodin',
    43200 => '12 hodin',
    86400 => '1 den',
};

our $VERSION = 1;

sub ttls {

    my $self = shift;
    my $args = shift;

    $args->{label_name} //= 'text';
    $args->{value_name} //= 'value';

    my @ttls = ();

    TTL:
    foreach my $ttl ( keys %{ TTL() } ) {

        push @ttls, {
            $args->{value_name} => $ttl,
            $args->{label_name} => TTL->{$ttl} . " ($ttl)",
        };

    }

    return [
        sort { $a->{$args->{value_name}} <=> $b->{$args->{value_name}} } @ttls
    ];
}

sub construct {
    my ( $self, $args ) = @_;

    my $rr   = $args->{rr};
    my $zone = $rr ? $rr->zone : $args->{zone};

    my $type = $rr ? $rr->type : $args->{type} || DEFAULT_TYPE;

    # formalni validace
    if ( exists $args->{ttl} ) {
        if ( $args->{ttl} !~ /^\d+$/) {
            return { error => ERROR_TTL_INVALID, field => 'ttl'};
        }

        if ( $args->{ttl} < TTL_MIN || $args->{ttl} > TTL_MAX) {
            return { error => ERROR_TTL_OVERRIDE, field => 'ttl'};
        }
    }

    if ( exists $args->{preference} ) {
        if ( $args->{preference} !~ /^\d+$/) {
            return { error => ERROR_PREFERENCE_INVALID, field => 'preference'};
        }

        if ( $args->{preference} < PREFERENCE_MIN || $args->{preference} > MAX_INT) {
            return { error => ERROR_PREFERENCE_OVERRIDE, field => 'preference'};
        }
    }


    # defaulty pro existujici zaznam
    if ( $rr ) {

        EXISTS:
        foreach my $field ( qw( preference weight port value ttl ) ) {
            if ( ! exists $args->{ $field} ) {
                my $exists = $rr->get_column( $field );
                $args->{ $field } = $exists if defined $exists;
            }
        }
    }

    my $name;

    if ( $rr ) {
        $name = $rr->name_idn;
    }
    elsif ( $args->{type} ne 'PTR') {
        $name = join '.', grep /./, ( lc( $args->{name} ),  $zone->name );
#        $name = $args->{name};
        $name =~ s/\*/X/g if $type eq 'CNAME'; # hack kvuli hvezdickovym zaznamum
        $name = $self->result_source->schema->idn_encode( $name );

        # standartni validace z povolenim '_'
        my $valid_name = is_domain( $name,
            {
                domain_allow_underscore => 1,
            }
        );

        # zprisnena validace pro srv
        if ( $type eq 'SRV' ) {
            $valid_name = 0 if $name !~ /^_[a-z0-9\-]+\._(tcp|udp|tls)/i;
        }

        if ( ! $valid_name ) {
            return { error => ERROR_INVALID_NAME, field => 'name'};
        }

        $name =~ s/X/\*/g;
    }

    my %rr = (
        type  => $type,
        name  => $name,
        class => $rr ? $rr->class : $args->{class} || DEFAULT_CLASS,
        ttl   => $args->{ttl} || DEFAULT_TTL,
    );

    if ( $type eq 'A' ) {

        if ( ! is_ipv4( $args->{value}) ) {
            return { error => ERROR_INVALID_IPV4, field => 'address' };
        }

        return Net::DNS::RR->new( %rr, address => $args->{value}, );
    }

    if ( $type eq 'AAAA' ) {

        if ( ! is_ipv6( $args->{value} ) ) {
            return { error => ERROR_INVALID_IPV6, field => 'address' };
        }

        return Net::DNS::RR->new( %rr, address => $args->{value}, );
    }

    if ( $type eq 'CNAME' ) {

        my $cname = $self->result_source->schema->idn_encode( $args->{value} );

        if ( ! is_domain( $cname, { domain_allow_underscore => 1 } ) ) {
            return { error => ERROR_INVALID_CNAME, field => 'cname' };
        }

        return Net::DNS::RR->new( %rr, cname => $cname, );
    }

    if ( $type eq 'MX' ) {

        my $exchange = $self->result_source->schema->idn_encode( $args->{value} );

        if ( ! is_domain( $exchange ) ) {
            return { error => ERROR_INVALID_MX, field => 'exchange' };
        }

        return Net::DNS::RR->new( %rr,
            preference => $args->{preference},
            exchange   => $exchange,
        );
    }

    if ( $type eq 'NS' ) {

        my $nsdname = $self->result_source->schema->idn_encode( $args->{value} );

        if ( ! is_domain( $nsdname ) ) {
            return { error => ERROR_INVALID_NS, field => 'nsdname' };
        }

        return Net::DNS::RR->new( %rr, nsdname => $nsdname, );
    }

    if ( $type eq 'TLSA' ) {

        my ($usage, $selector, $matchingtype, $certificate) = split /\s+/, $args->{value};

        if ( length($args->{value} > MAX_TLSA_SIZE) ) {
            return { error => ERROR_INVALID_TLSA_SIZE, field => 'tlsadata' };
        }

        if ( $usage !~ /^[0123]$/ ) {
            return { error => ERROR_INVALID_TLSA_USAGE, field => 'tlsadata' };
        }

        if ( $selector !~ /^[01]$/ ) {
            return { error => ERROR_INVALID_TLSA_SELECTOR, field => 'tlsadata' };
        }

        if ( $matchingtype !~ /^[012]$/ ) {
            return { error => ERROR_INVALID_TLSA_MATCHINGTYPE, field => 'tlsadata' };
        }

        return Net::DNS::RR->new( %rr,
            usage        => $usage,
            selector     => $selector,
            matchingtype => $matchingtype,
            certificate  => $certificate
        );
    }

    if ( $type eq 'SRV' ) {

        my $target = $self->result_source->schema->idn_encode( $args->{value} );

        if ( ! is_domain( $target ) ) {
            return { error => ERROR_INVALID_SRV_TARGET, field => 'target' };
        }

        if ( ( $args->{weight} !~ /^\d+$/ ) || $args->{weight} > MAX_INT ) {
            return { error => ERROR_INVALID_SRV_WEIGHT , field => 'weight' };
        }

        if ( ( $args->{port} !~ /^\d+$/ ) || $args->{port} > MAX_INT ) {
            return { error => ERROR_INVALID_SRV_PORT , field => 'port' };
        }

        return Net::DNS::RR->new( %rr,
            priority => $args->{priority} // $args->{preference},
            weight   => $args->{weight},
            port     => $args->{port},
            target   => $target,
        );
    }

    if ( $type eq 'DS' ) {

        my $digest = $args->{value};
        $digest =~ s/\s+//g;

        if (
            $digest !~ /^[\da-f]+$/i
            ||
            ($args->{digtype} == 1 && length($digest) != 40 )
            ||
            ( $args->{digtype} == 2 && length($digest) != 64 )
        ) {
            return { error => ERROR_BAD_DIGEST, field => 'digest' };
        }

        return Net::DNS::RR->new( %rr,
            keytag    => $args->{keytag},
            algorithm => $args->{algorithm},
            digtype   => $args->{digtype},
            digest    => $digest,
        );
    }


    if ( $type eq 'PTR' ) {

        my $ptrdname = $self->result_source->schema->idn_encode( $args->{value} );

        if ( ! is_domain( $ptrdname ) ) {
            return { error => ERROR_INVALID_DOMAIN, field => 'ptrdname' };
        }

        my $network = Net::IP->new( $zone->network );

        my $address;

        if ( $rr && $rr->address ) {
            $address = Net::IP->new( $rr->address );
        }
        elsif ( $zone->type == 3 ) {
            $address = $network->short . '.' . $args->{name};
            $address = Net::IP->new( $address, 4 )
                || return { error => ERROR_INVALID_IPV4 , field => 'name' };
        }
        elsif ( $zone->type == 4 ) {
            my $prefix = $zone->prefix_ipv6();
            $address = $args->{name};
            if (index($address, $prefix) != 0) {
                $address = $prefix . $address;
            }

            $address = Net::IP->new( $address, 6 )
                || return { error => ERROR_INVALID_IPV6, field => 'name' };

        }

        return (
            Net::DNS::RR->new( %rr,
                name     => $address->reverse_ip,
                ptrdname => $ptrdname,
            ),
            {
                address => $address->ip,
            }
        );
    }

    if ( $type eq 'TXT' ) {
        return Net::DNS::RR->new( %rr,
            txtdata => [
                unpack("(A255)*", $args->{value})
            ],
        );
    }

    return Net::DNS::RR->new(
        join ' ', (
            $rr{name},
            $rr{ttl},
            $rr{class},
            $rr{type},
            $args->{value},
        ),
    );

}

sub parse {
    my $self   = shift;
    my $string = shift;

    my $RR;

    eval {
        $RR = Net::DNS::RR->new( $string );
    };

    return { error => $@ } if $@;

    my $schema = $self->result_source->schema;

    my %rr = (
        type  => $RR->type,
        class => $RR->class,
        ttl   => $RR->ttl,
        name  => $schema->idn_decode($RR->name),
    );

    if ( $RR->type eq 'MX' ) {
        return {
            %rr,
            value      => $RR->exchange,
            preference => $schema->idn_decode($RR->preference),

        };
    }
    elsif ( $RR->type eq 'SRV' ) {
        return {
            %rr,
            value      => $schema->idn_decode($RR->target),
            preference => $RR->priority,
            weight     => $RR->weight,
            port       => $RR->port,

        };
    }
    else { # TODO: upresnit
        my $value = $RR->string;

        my $class = $RR->class;
        my $type  = $RR->type;

        $value =~ s/^.+\s+$class\s+$type\s+//;
        $value =~ s/\.$//;

        return {
            %rr,
            value => $schema->idn_decode($value),
        };
    }
}
__END__
