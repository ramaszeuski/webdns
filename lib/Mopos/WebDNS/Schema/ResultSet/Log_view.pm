package Mopos::WebDNS::Schema::ResultSet::Log_view;

use strict;
use warnings;
use utf8;

use base 'DBIx::Class::ResultSet';

our $VERSION = 1;

use constant EVENT => {
    user_login      => 'Přihlásil se',
    user_logout     => 'Odhlásil se',
    user_create     => 'Přidal uživatele',
    user_edit       => 'Editoval uživatele',
    user_delete     => 'Smazal uživatele',

    change_password => 'Změnil heslo',

    customer_create => 'Přidal zákazníka',
    customer_edit   => 'Editoval zákazníka',
    customer_delete => 'Smazal zákazníka',

    rr_create       => 'Přidal záznam',
    rr_edit         => 'Editoval záznam',
    rr_delete       => 'Smazal záznam',
    rr_change_owner => 'Změnil vlastnika záznamu',

    zone_create     => 'Přidal doménu',
    zone_update     => 'Editoval doménu',
    zone_delete     => 'Smazal doménu',
    zone_activate   => 'Aktivoval doménu',
    zone_deactivate => 'Deaktivoval doménu',

    range_create    => 'Přidal rozsah',

    dnssec_enabled  => 'Zapnul DNSSEC',
    dnssec_disabled => 'Vypnul DNSSEC',
};

sub events {
    return [
        map {
            { value => $_, label => EVENT->{$_} }
        } ( sort keys %{ EVENT() } )
    ];
}

sub event_name {
    my $self  = shift;
    my $event = shift;
    return EVENT->{$event};
}

1;
