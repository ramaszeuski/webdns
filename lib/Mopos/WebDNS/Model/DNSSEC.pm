package Mopos::WebDNS::Model::DNSSEC;
use Moose;
use namespace::autoclean;
use Net::DNS;
use Net::DNS::SEC;
use Net::DNS::RR::DNSKEY;
#use Net::DNS::Keyset;
use Date::Manip;
use File::Slurp;
use File::Copy;
use Digest::SHA;
use YAML;

$ENV{TZ} = 'UTC';

extends 'Catalyst::Model';

__PACKAGE__->config(
    Mopos::WebDNS->config->{dnssec},
);

use constant DEFAULT_TTL_DNSKEY => 3600;
use constant DEFAULT_TTL_DS     => 3600;
use constant GENERATE_KSK_CMD   => '/usr/sbin/dnssec-keygen -r /dev/urandom -a %s -b %d -K %s -L %s -f KSK %s';
use constant GENERATE_ZSK_CMD   => '/usr/sbin/dnssec-keygen -r /dev/urandom -a %s -b %d -K %s -L %s -I +%s -D +%s %s';
use constant ROLLOVER_ZSK_CMD   => '/usr/sbin/dnssec-keygen -r /dev/urandom -a %s -b %d -K %s -L %s -P now -A %s -I %s -D %s %s';
use constant SETTTME_CMD        => '/usr/sbin/dnssec-settime -K %s -%s %s %s';
use constant FAILSAFE_TLD       => 'default';

use constant TIMES              => {
    mi => 60,
    h  => 60*60,
    d  => 60*60*24,
    w  => 60*60*24*7,
    mo => 60*60*24*30,
    y  => 60*60*24*365,
};

use constant NSEC3_CAPABLE_ALGORITHMS => [qw(
    DSA-NSEC3-SHA1
    RSASHA1-NSEC3-SHA1
    RSASHA256
    RSASHA512
    ECC-GOST
    ECDSAP256SHA256
    ECDSAP384SHA384
)];

use constant DS_ALGORITHMS => [qw(
    SHA1
    SHA256
    SHA384
)];

# nektere algoritmny dnssec-keygen pojmenava jinak
use constant KEYGEN_ALGORITHM => {
    'RSASHA1-NSEC3-SHA1' => 'NSEC3RSASHA1',
    'DSA-NSEC3-SHA1' => 'NSEC3DSA',
    'ECC-GOST' => 'ECCGOST',
};

my %nsec3 = map {
    $_ => 1
} @{ NSEC3_CAPABLE_ALGORITHMS() };

my %algorithms = _load_algorithms();

__PACKAGE__->mk_accessors(qw(log));

sub ACCEPT_CONTEXT {
    my ( $self, $c ) = @_;
    $self = bless({ %$self,
        log => $self->log,
    }, ref($self));
    return $self;
}

sub trace {
    my $self  = shift;
    my $msg   = shift;
    my $level = shift;

    if ( $self->can('logger') ) {
        $self->logger->debug($msg);
    }
    else {
        print "$msg\n";
    }
}

# Seznam algoritmu
sub algorithms {
    my $self   = shift;
    my $filter = shift;

    # Mozne polozky filtra:
    #   key_type (ksk|zsk)
    #   algorithm
    #   size_limit

    my $algorithms = $algorithms{ $filter->{key_type} // 'ksk' };
    my @algorithms = ();

    ALGORITHM:
    foreach my $algorithm (values %{ $algorithms }) {

        if (
            $filter->{algorithm}
            &&
            $algorithm->{algorithm} ne $filter->{algorithm}
        ) {
            next ALGORITHM;
        }

        if (
            $filter->{size_limit}
            &&
            $algorithm->{keysize} >= $filter->{size_limit}
        ) {
            next ALGORITHM;
        }

        push @algorithms, $algorithm,
    }

    return sort {
        $a->{algorithm} cmp $b->{algorithm}
        ||
        $a->{keysize} <=> $b->{keysize}
    } @algorithms;
}

# seznam tld
sub tlds {
    my $self   = shift;

    my $tlds = $self->config->{tlds};
    my @tlds = ();

    TLD:
    foreach my $id (sort keys %{ $tlds }) {
        push @tlds, {
            id    => $id,
            label => $tlds->{$id}{label} // $id,
        }
    }

    return @tlds;

}

sub get_keys { # keys je kolizni!
    my $self   = shift;
    my $filter = shift;
    my $args   = shift;

    # Mozne polozky filtra:
    #   type (ksk|zsk)
    #   id
    #   name
    #   only_tld
    #   not_inactive
    #   not_deleted
    #   deleted
    #   trash

    # with_digest - ke kazdemu souoru vypocitat digest

    my @keys;
    my ( $dh, $fh );

    opendir($dh, $self->config->{directory});

    KEY:
    foreach my $file (readdir($dh)) {
        # jenom spravny format

        next KEY if $file !~ /^K(.+)\.\+(\d+)\+(\d+)\.key/;
        next KEY if $filter->{id} && $filter->{id} != $3;

#        $self->trace("Loading KEY $file");

        my $key = {
            file  => $file,
        };

        open ($fh, $self->config->{directory} . '/' . $file);
        while ( my $line = <$fh> ) {
            # TODO: plnohodnotny parsing timestampu
            if ($line =~ /^;\s+(\w+):\s+(20\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)\s+\(/) {
                $key->{timestamp}{ lc($1) } = "$2-$3-$4 $5:$6:$7";
            }

            if ($line =~ /IN\s+DNSKEY\s+(\d+)\s+(\d+)\s+(\d+)\s+(.+)$/ ) {
                my $rr = Net::DNS::RR->new($line);

                $key->{rr}        = $rr;
                $key->{algorithm} = Net::DNS::RR::DNSKEY->algorithm($rr->algorithm);
                $key->{type}      = $rr->sep ? 'ksk' : 'zsk';
                $key->{keysize}   = $rr->keylength;
                $key->{name}      = $rr->owner;
                $key->{id}        = $rr->keytag;
            }
        }
        close $fh;

        next KEY if $filter->{type} && $filter->{type} ne $key->{type};
        next KEY if $filter->{name} && $filter->{name} ne $key->{name};
        next KEY if $filter->{only_tld}
                 && ! exists $self->config->{tlds}{$key->{name}};

        my $algorithm = $algorithms{$key->{type}}{ _algorithm_id($key) };

        my $lifetime = _lifetime_sec( $algorithm->{lifetime} );

        $key->{lifetime} = $lifetime;

        my $now = UnixDate('now', '%Y-%m-%d %H:%M:%S');

# bezvyznamny?
        $key->{expire} = DateCalc( $key->{timestamp}{activate}, '+' . $lifetime . 's' );
        $key->{is_expired} = (Date_Cmp($key->{expire}, $now) > 0) ? 0 : 1;
        $key->{expire} = Date_ConvTZ($key->{expire},'UTC','CET');
        $key->{expire} = UnixDate( $key->{expire}, '%Y-%m-%d %H:%M:%S' );

        if ( exists $key->{timestamp}{inactive}) {
            $key->{is_inactive} = (Date_Cmp( $key->{timestamp}{inactive}, $now ) > 0) ? 0 : 1;
            $key->{rollover} = DateCalc( $key->{timestamp}{inactive}, '-' . int($lifetime/3) .'s'  );
            $key->{rollover} = UnixDate( $key->{rollover}, '%Y-%m-%d %H:%M:%S' );
            $key->{rollover_required} = (Date_Cmp( $key->{rollover}, $now ) > 0) ? 0 : 1;
        }
        else {
            $key->{is_inactive} = 0;
        }

        next KEY if $filter->{not_inactive} && $key->{is_inactive};

        if ( exists $key->{timestamp}{delete}) {
            $key->{is_deleted} = (Date_Cmp( $key->{timestamp}{delete}, $now ) > 0) ? 0 : 1;
            #TODO: do konfigurace nebo promenne
            $key->{is_trash}   = (Date_Cmp( DateCalc($key->{timestamp}{delete}, '+10min'), $now ) > 0) ? 0 : 1;
        }
        else {
            $key->{is_deleted} = 0;
        }

        next KEY if $filter->{not_deleted} && $key->{is_deleted};
        next KEY if $filter->{deleted} && ! $key->{is_deleted};
        next KEY if $filter->{trash} && ! $key->{is_trash};

        TIMESTAMP:
        foreach my $timestamp (qw(created publish activate inactive delete)) {
            next TIMESTAMP if ! exists $key->{timestamp}{ $timestamp };
            $key->{local_timestamp}{ $timestamp } = UnixDate(
                Date_ConvTZ($key->{timestamp}{ $timestamp },'UTC','CET'),
                '%Y-%m-%d %H:%M:%S',
            );
        }

        if ( $args->{with_digest} ) {
            my $sha = Digest::SHA->new();
            $sha->addfile(
                $self->config->{directory} . '/' . $key->{rr}->privatekeyname()
            );
            $key->{digest} = $sha->hexdigest;
        }

        push @keys, $key;
    }

    closedir $dh;

# SORT
# LIMIT

    return @keys;

}

sub keysets {
    my $self = shift;
    my $zone = shift;

    if ( my $tld = $self->config->{tlds}{$zone->parent_name()} ) {
        return $tld->{keysets};
    }
}

sub ksk_generate {
    my $self = shift;
    my $args = shift;

    my $params = $algorithms{ksk}{ $args->{algorithm} };

    my $cmd = sprintf (GENERATE_KSK_CMD,
        _keygen_algorithm($params->{algorithm}),
        $params->{keysize},
        $self->config->{directory},
        $self->config->{ttl}{dnskey} // DEFAULT_TTL_DNSKEY,
        $args->{tld},
    );

    $self->trace($cmd);

    my $rc = `$cmd`;
}

sub ksk_clone {
    my $self = shift;
    my $zone = shift;

    my $zone_name = $zone->name;
    my $tld_name  = $zone->parent_name;

    if ( ! exists $self->config->{tlds}{ $tld_name} ) {
        $tld_name = FAILSAFE_TLD;
    }

    my @tld_ksks = $self->get_keys(
        {
            type => 'ksk',
            name => $tld_name,
        }
    );

    my $prefix = $self->config->{directory} . '/';

    TLD_KSK:
    foreach my $tld_ksk ( @tld_ksks ) {
        my $ksk     = $tld_ksk->{file};
        my $private = $tld_ksk->{rr}->privatekeyname;

        $ksk     =~ s/^K$tld_name(.+)/K$zone_name$1/;
        $private =~ s/^K$tld_name(.+)/K$zone_name$1/;

        # privatni klic je beze zmen - staci jenom symlink
        copy(
            $prefix . $tld_ksk->{rr}->privatekeyname,
            $prefix . $private,
        );
        chmod 0644, $prefix . $private; # stacilo by 0640

        # verejny klic vyzaduje nahradu jmena zony
        open (my $in,  '<', $prefix . $tld_ksk->{file});
        open (my $out, '>', $prefix . $ksk);

        LINE:
        while ( my $line = <$in> ) {
            $line =~ s/^$tld_name(\. .+)/$zone_name$1/;
            $line =~ s/(.+ for )$tld_name\.$/$1$zone_name\./;
            print $out $line;
        }

        close $out;
        close $in;
    }
}

sub zsk_generate {
    my $self = shift;
    my $zone = shift;

    my %generated = ();
    my $count     = 0;

    # Abychom neregenerovali existujici
    my @zsks = $self->get_keys(
        {
            type        => 'zsk',
            name        => $zone->name,
            not_inactie => 1,
            not_deleted => 1,
        },
    );
    ZSK:
    foreach my $zsk ( @zsks ) {
        $generated{ $zsk->{algorithm} }++;
    }

    my $tld_name  = $zone->parent_name;
    if ( ! exists $self->config->{tlds}{ $tld_name} ) {
        $tld_name = FAILSAFE_TLD;
    }

    my @ksks = $self->get_keys(
        {
            type        => 'ksk',
            name        => $tld_name,
            not_inactie => 1,
            not_deleted => 1,
        }
    );
    # TODO: jenom validni ZSK!!!
#    my @ksks = $self->get_keys(
#        {
#            type => 'ksk',
#            name => $zone->name,
#        },
#    );

    KSK:
    foreach my $ksk ( @ksks ) {
        next KSK if $generated{ $ksk->{algorithm} }++;

        my @algorithms = $self->algorithms(
            {
                key_type  => 'zsk',
                algorithm => $ksk->{algorithm},
            }
        );

        next KSK if ! scalar @algorithms;
        my $algorithm = shift @algorithms;

        my $lifetime   = _lifetime_sec($algorithm->{lifetime});
        my $inactivate = $lifetime;
        my $delete     = int($lifetime * 4 / 3);

        my $cmd = sprintf (GENERATE_ZSK_CMD,
            _keygen_algorithm($algorithm->{algorithm}),
            $algorithm->{keysize},
            $self->config->{directory},
            $self->config->{ttl}{dnskey} // DEFAULT_TTL_DNSKEY,
            $inactivate,
            $delete,
            $zone->name,
        );

        $self->trace($cmd);

        my $key = `$cmd`;
        $key =~ s/\s+//gs;
        $count++;

        # inline signing  vyzaduje pristup bind
        chmod 0644, $self->config->{directory} . "/$key.private";

    }
    return $count;
}

sub zsk_generate_sucessors {
    my $self = shift;
    my $zone = shift;
    my $args = shift;

    # Ziskame KSK pro zonu
    my %allowed_algorithms = map {
        $_->{algorithm} => 1,
    } $self->get_keys(
        {
            type        => 'ksk',
            name        => $zone->name_idn,
            not_inactie => 1,
            not_deleted => 1,
        }
    );

    my @keys = sort {
        $b->{timestamp}{inactive} cmp $a->{timestamp}{inactive};
    } ($self->get_keys(
        {
            type     => 'zsk',
            name     => $zone->name,
        }
    ));

    my %generated = ();
    my $count     = 0;

    KEY:
    foreach my $key ( @keys ) {
        # nepokracovat v pripade ze neni KSK
        next KEY if ! exists $allowed_algorithms{ $key->{algorithm} };
        # jen nejstarsi klic pro kazdy algoritmus
        next KEY if $generated{ $key->{algorithm} }++;
        next KEY if ! $key->{rollover_required};

        my $algorithm = $self->config->{algorithms}{ $key->{algorithm} };

        my $lifetime  = _lifetime_sec($algorithm->{lifetime}{zsk});

        my $cmd;

        if ($key->{is_inactive}) {
            # neaktivni klic - vygenerovat uplne novy
            my $inactivate = $lifetime;
            my $delete   = int($lifetime * 4 / 3);

            $cmd = sprintf (GENERATE_ZSK_CMD,
                _keygen_algorithm($key->{algorithm}),
                $key->{keysize},
                $self->config->{directory},
                $self->config->{ttl}{dnskey} // DEFAULT_TTL_DNSKEY,
                $inactivate,
                $delete,
                $zone->name,
            );
        }
        else {
            my $activate   = $key->{timestamp}{inactive};
            my $inactivate = DateCalc($activate, '+' . $lifetime . 's');
            my $delete     = DateCalc($activate, '+' . int($lifetime * 4 / 3) . 's');

            $cmd = sprintf (ROLLOVER_ZSK_CMD,
                _keygen_algorithm($key->{algorithm}),
                $key->{keysize},
                $self->config->{directory},
                $self->config->{ttl}{dnskey} // DEFAULT_TTL_DNSKEY,
                _metadata_ts($activate),
                _metadata_ts($inactivate),
                _metadata_ts($delete),
                $zone->name,
            );
        }

        $self->trace($cmd);

        my $key = `$cmd`;
        $key =~ s/\s+//gs;

        # inline signing  vyzaduje pristup bind
        chmod 0644, $self->config->{directory} . "/$key.private";

        $count++;
    }

    return $count;
}

sub get_key {
    my $self = shift;
    my $id   = shift;
    my $args = shift // {};

    my @keys = $self->get_keys(
        {
            id => $id,
            %{ $args },
        }
    );

    my $key = $keys[0];
    return {} if ! $key;

    DIGEST:
    foreach my $digtype ( @{ DS_ALGORITHMS() }) {
        my $ds = Net::DNS::RR::DS->create(
            $key->{rr},
            digtype => $digtype,
            ttl     => $self->config->{ttl}{ds} // DEFAULT_TTL_DS,
        );

        push @{ $key->{ds} }, $ds;
    }

    return $key;

}

sub settime_key {
    my $self = shift;
    my $key  = shift;
    my $args = shift;

    TIMESTAMP:
    foreach my $timestamp ( keys %{ $args->{timestamps} } ) {
        my $meta = $args->{timestamps}{ $timestamp }
                 ? _metadata_ts( $args->{timestamps}{ $timestamp } )
                 : 'none'
                 ;

        my $cmd = sprintf( SETTTME_CMD,
            $self->config->{directory},
            $timestamp,
            $meta,
            $key->{file}
        );

        $self->trace($cmd);
        system($cmd);

    }

    chmod 0644, $self->config->{directory} . '/' .$key->{rr}->privatekeyname;
}

sub delete_key {
    my $self = shift;
    my $key = shift;

    # jeste jednou overit existenci DS zaznamu?
    my $prefix = $self->config->{directory} . '/';

    unlink $prefix . $key->{file};
    unlink $prefix . $key->{rr}->privatekeyname;
}

sub delete_keys {
    my $self = shift;
    my $zone = shift;
    my $args = shift;

    my @keys = $self->get_keys(
        {
            type => $args->{type},
            name => $zone->name,
        }
    );

    KEY:
    foreach my $key ( @keys ) {
        $self->delete_key( $key );
    }

}

sub dsset {
    my $self = shift;
    my $zone = shift;

    my @dsset = ();

    my @keys = $self->get_keys(
        {
            type        => 'ksk',
            name        => $zone->name_idn,
            not_deleted => 1,
        },
    );

    KEY:
    foreach my $key ( @keys ) {
        my $ds = Net::DNS::RR::DS->create(
            $key->{rr},
            digtype => 'SHA256',
            ttl     => $self->config->{ttl}{ds} // DEFAULT_TTL_DS,
        );

        push @dsset, $ds;
    }

    return @dsset;

}

sub _algorithm_id {
    my $algorithm = shift;
    return $algorithm->{algorithm} . ' - ' . $algorithm->{keysize};
}

sub _keygen_algorithm {
    my $algorithm = shift;
    return KEYGEN_ALGORITHM()->{$algorithm} || $algorithm;
}

sub _lifetime_sec {
    my $lifetime = shift;

    if ($lifetime =~ /(\d+)(mi|h|d|w|mo|y)/i) {
        $lifetime = $1 * TIMES->{$2};
    }
    else {
        $lifetime =~ s/\D+//g;
    }

    return $lifetime;

}

sub _metadata_ts {
    my $timestamp = shift;
    return UnixDate( $timestamp, '%Y%m%d%H%M%S');
}

sub _load_algorithms {

    my %src        = %{ __PACKAGE__->config->{algorithms} };
    my %algorithms = ();


    ALGORITHM:
    foreach my $name ( keys %src ) {

        # povolujeme jen pouzitelne z NSEC3
        next ALGORITHM if ! exists $nsec3{ $name };

        # Validace algoritmu
        eval { Net::DNS::RR::DNSKEY->algorithm($name) };
        next ALGORITHM if $@;

        my $algorithm = $src{$name};

        my @keysizes;

        if ( ref $algorithm->{keysize} ) {
            @keysizes = @{ $algorithm->{keysize} };
        }
        else {
            @keysizes = ( $algorithm->{keysize} );
        }

        @keysizes = sort { $a <=> $b } grep {/^\d{3,4}$/} keys %{{ map { $_ => 1 } @keysizes }};

        my $min_keysize = $keysizes[0];
        my $max_keysize = $keysizes[ $#keysizes ];

        KEYSIZE:
        foreach my $keysize ( @keysizes ) {

            TYPE:
            foreach my $type ( qw(ksk zsk) ) {

                my $lifetime = $algorithm->{lifetime}{$type};
                next TYPE if ! $lifetime;

                if ( ( scalar @keysizes ) > 1 ) {
                    next TYPE if $type eq 'ksk' && $keysize == $min_keysize;
                    next TYPE if $type eq 'zsk' && $keysize == $max_keysize;
                }

                my $id = _algorithm_id (
                    {
                        algorithm => $name,
                        keysize   => $keysize,
                    }
                );

                $algorithms{$type}{$id} = {
                    id               => $id,
                    algorithm        => $name,
                    keysize          => $keysize,
                    lifetime         => $lifetime,
                };
            }
        }
    }

    return %algorithms;
}


=head1 NAME

Mopos::WebDNS::Model::DNSSEC - Catalyst Model

=head1 DESCRIPTION

Catalyst Model.


=encoding utf8

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

__END__
