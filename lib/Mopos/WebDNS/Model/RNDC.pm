package Mopos::WebDNS::Model::RNDC;
use Moose;
use namespace::autoclean;

use Template;
use Net::RNDC;
use File::Copy;
use File::Find;
use File::Temp qw( tempfile );
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use Net::OpenSSH;
use YAML;

extends 'Catalyst::Model';

use constant ZONE_NAME_FORMAT   => 'db.%s';
use constant ZONE_FILE          => '%s/db.%s';
use constant TEMPLATE_ZONE      => 'bind.zone.tt2';
use constant DEFAULT_PORT       => 953;
use constant NSEC3PARAM         => 'signing -nsec3param 1 0 10 %s %s';
use constant ZONEFILE_SUFFIXES  => ['.jbk', '.jnl', '.signed', '.signed.jnl'];

__PACKAGE__->config(
    dnssec    => Mopos::WebDNS->config->{dnssec},
    output    => Mopos::WebDNS->config->{zones},
    servers   => Mopos::WebDNS->config->{nameservers},
);

__PACKAGE__->mk_accessors(qw(log));

sub ACCEPT_CONTEXT {
    my ( $self, $c ) = @_;
    $self = bless({ %$self,
        log => $self->log,
    }, ref($self));
    return $self;
}

sub trace {
    my $self  = shift;
    my $msg   = shift;
    my $level = shift;

    if ( $self->can('logger') ) {
        $self->logger->debug($msg);
    }
    else {
        print "$msg\n";
    }
}
our $tt = Template->new({
    INCLUDE_PATH => Mopos::WebDNS->config->{templates},
    INTERPOLATE  => 1,
}) || die "$Template::ERROR\n";

sub add_zone {
    my ( $self, $zone, $c ) = @_;

    if ( $zone->type != 2 ) {
        $zone->update_serial();
    }

    if ( $zone->active ) {
        $self->activate_zone( $zone, { context => $c } );
    }
}

sub update_zone {
    my ($self, $zone, $args ) = @_;

    my ( $stealth ) = $self->_servers();

    my $serial = $self->get_serial( $zone );

    $zone->update_serial( $serial );
    return if ! $zone->active; #TODO: mozna pred aktualizaci serialu

    $self->make_zone_file( $zone, $args );

    my $rndc = Net::RNDC->new(
        host => $stealth->{address},
        port => $stealth->{port} || DEFAULT_PORT,
        key  => $stealth->{key},
    );

    $rndc->do( 'reload ' . $zone->name_idn );
}

sub get_serial {
    my ($self, $zone ) = @_;

    my ( $stealth ) = $self->_servers();

    my $serial   = 0;
    my $resolver = new Net::DNS::Resolver(
        nameservers => [$stealth->{address}],
    );
    my $reply    = $resolver->query( $zone->name_idn, 'SOA' );

    if ($reply) {
        my @answer = $reply->answer();
        $serial = $answer[0]->serial();
    }

    return $serial;
}

sub delete_zone {
    my ($self, $zone) = @_;

    my ( $stealth, @slaves ) = $self->_servers();

    SERVER:
    foreach my $server ( $stealth, @slaves ) {

        if ( $server->{protocol} eq 'rndc' ) { # TODO: plugins

            my $rndc = Net::RNDC->new(
                host => $server->{address},
                port => $server->{port} || DEFAULT_PORT,
                key  => $server->{key},
            );

            $rndc->do( 'delzone ' . $zone->name_idn );

        }
    }
}

sub reconfigure_zone {
    my $self = shift;
    $self->deactivate_zone(@_);
    $self->activate_zone(@_);
}

sub load_zone_keys {
    my ($self, $zone, $args ) = @_;

    my ( $stealth, @slaves ) = $self->_servers();

    my $rndc = Net::RNDC->new(
        host => $stealth->{address},
        port => $stealth->{port} || DEFAULT_PORT,
        key  => $stealth->{key},
    );

    $rndc->do( 'loadkeys ' . $zone->name_idn );
}

sub activate_zone {
    local $YAML::UseHeader = 0;

    my ( $self, $zone, $args ) = @_;

    $self->make_zone_file( $zone, $args );

    my ( $stealth, @slaves ) = $self->_servers();
    my $c = $args->{context};

    SERVER:
    foreach my $server ( @slaves, $stealth ) {

        if (
            defined $args->{server}
            &&
            $args->{server}{role} eq 'slave'
            &&
            $args->{server}{address} ne $server->{address}
        ) {
            next SERVER;
        }

        # TODO: plugins
        if ( $server->{protocol} eq 'rndc' ) {

            my $add_command;

            $tt->process(
                'zone_config_' . $server->{role} . '.tt2',
                {
                    zone     => $zone,
                    server   => $server,
                    stealth  => $stealth,
                    slaves   => \@slaves,
                    filename => sprintf( ZONE_NAME_FORMAT, $zone->name_idn ),
                },
                \$add_command
            ) || die $tt->error(), "\n";

            $add_command =~ s/\n+/ /gs;
            $add_command =~ s/\s+/ /g;

            my $rndc = Net::RNDC->new(
                host => $server->{address},
                port => $server->{port} || DEFAULT_PORT,
                key  => $server->{key},
            );

            if ( ! $rndc->do( $add_command )) {
                $c && $c->log->error(
                    $server->{address} . ' ERROR: ' . $rndc->error
                );
            }

            # dnssec parametry jen na stealth
            next SERVER if $server->{role} ne 'stealth';

            if ( $zone->dnssec ) {
                my $nsec3param = sprintf(
                    NSEC3PARAM,
                    _salt(),
                    $zone->name,
                );
                if ( ! $rndc->do( $nsec3param )) {
                    $c && $c->log->error(
                        $server->{address} . ' ERROR: ' . $rndc->error
                    );
                }
            }

        }
        elsif ( $server->{protocol} eq 'knotc' ) {
            next SERVER if $server->{role} ne 'primary';
            next SERVER if ! exists $server->{dir_configs};

            my $local    = "/tmp/" . $zone->name_idn . '.conf';
            my $remote   = $server->{dir_configs} . $zone->name_idn . '.conf';
            my ( $ssh, $channel, $status);

            $ssh = Net::OpenSSH->new($server->{address},
                user     => $server->{user},
                key_path => $server->{private_key},
            );

            if ( $ssh->error ) {
                #TODO: log error
                next SERVER;
            }

            YAML::DumpFile($local, { zone => [{ domain => $zone->name_idn }] });
            $ssh->scp_put( $local, $remote);

            $status = $ssh->system('sudo ' . $server->{knotc_binary} . ' conf-check' );

            if ( ! $status ) {
                $ssh->system('sudo ' . $server->{knotc_binary} . ' reload' );
            } else {
                #TODO: log error
            }

            $ssh->disconnect;
            unlink($local);
        }
    }
}

sub deactivate_zone {
    my ($self, $zone, $args) = @_;

    my ( $stealth, @slaves ) = $self->_servers();

    SERVER:
    foreach my $server ( $stealth, @slaves ) {

        if (
            defined $args->{server}
            &&
            $args->{server}{role} eq 'slave'
            &&
            $args->{server}{address} ne $server->{address}
        ) {
            next SERVER;
        }

        if ( $server->{protocol} eq 'rndc' ) { # TODO: plugins

            my $rndc = Net::RNDC->new(
                host => $server->{address},
                port => $server->{port} || DEFAULT_PORT,
                key  => $server->{key},
            );

            $rndc->do( 'delzone ' . $zone->name_idn );
        }
        elsif ( $server->{protocol} eq 'knotc' ) {
            next SERVER if $server->{role} ne 'primary';
            next SERVER if ! exists $server->{dir_configs};

            my $remote   = $server->{dir_configs} . $zone->name_idn . '.conf';
            my $zonefile = $server->{dir_zones} . $zone->name_idn . '.zone';
            my ( $ssh, $channel, $status);

            $ssh = Net::OpenSSH->new($server->{address},
                user     => $server->{user},
                key_path => $server->{private_key},
            );

            if ( $ssh->error ) {
                #TODO: log error
                next SERVER;
            }

            $ssh->sftp->remove( $remote );

            $status = $ssh->system('sudo ' . $server->{knotc_binary} . ' conf-check' );

            if ( ! $status ) {
                $ssh->system('sudo ' . $server->{knotc_binary} . ' reload' );
            } else {
                #TODO: log error
            }

            $ssh->sftp->remove( $zonefile );
            $ssh->disconnect;
        }
    }

    if ( $args->{wipe} ) {
        my $fullname = sprintf( ZONE_FILE, $self->config->{output}, $zone->name_idn );

        SUFFIX:
        foreach my $suffix ('', @{ZONEFILE_SUFFIXES()}) {
            next SUFFIX if ! -f $fullname . $suffix;
            unlink $fullname . $suffix;
        }
    }


}

sub zip {
    my ($self, $zones) = @_;

    my ($fh, $filename) = tempfile();

    my $zip = Archive::Zip->new( );

    ZONE:
    while ( my $zone = $zones->next ) {

        my $name = sprintf( ZONE_NAME_FORMAT, $zone->name_idn );
        $name .= '-' . $zone->id if ! $zone->active;

        if ( $zone->type == 2 && ! $zone->active ) {
            my $content = join "\n", map { $_->string } $zone->get_secondary();
            next ZONE if ! $content;
            $zip->addString( $content, $name );
        }
        elsif ( ! $zone->active ) {
            my $content = $zone->string();
            next ZONE if ! $content;
            $zip->addString( $content, $name );
        }
        else {
            my $file = sprintf( ZONE_FILE, $self->config->{output}, $zone->name_idn );
            next ZONE if ! -r $file;
            $zip->addFile( $file, $name );
        }

    }

    $zip->writeToFileHandle( $fh );
    return $filename;

}

sub make_zone_file {
    my ($self, $zone, $args) = @_;

    my $c = $args->{context};

    if ( ! -w $self->config->{output} ) {
        $c && $c->log->error(
            sprintf 'Directory %s is not writable',
            $self->config->{output},
        );
        return;
    }

    my $fullname = sprintf( ZONE_FILE, $self->config->{output}, $zone->name_idn );
    $c && $c->log->debug( "Make zone file $fullname" );

    $tt->process(
        TEMPLATE_ZONE,
        {
            zone => $zone,
        },
        $fullname,
    ) || die $tt->error(), "\n";

}

sub _salt {
    open RND, "/dev/urandom";
    read RND, my $bytes, 32;
    close RND;
    return substr(unpack("H*", $bytes), 0, 16);
}


sub _servers {
    my $self = shift;

    my ( $stealth, @slaves ) = ();

    SERVER:
    foreach my $server ( @{ $self->config->{servers} } ) {
        if ( $server->{role} eq 'stealth' ) {
            $stealth = $server;
        }
        else {
            push @slaves, $server;
        }
    }

    return ( $stealth, @slaves );
}


=head1 NAME

Mopos::WebDNS::Model::RNDC - Catalyst Model

=head1 DESCRIPTION

Catalyst Model.

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
