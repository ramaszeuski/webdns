package Mopos::WebDNS::Schema;

use strict;
use warnings;

use base 'DBIx::Class::Schema';
use IDNA::Punycode;

sub idn_encode {
    my $self = shift;
    my $string = shift;
    $string = join ".",
            map { encode_punycode($_) }
            split /\./, $string;
    $string =~ s/xn---/xn--/g; # BUG encoderu?
    return $string;
};

sub idn_decode {
    my $self = shift;
    my $string = shift;
    return join ".",
            map { decode_punycode($_) }
            split /\./, $string;
};

__PACKAGE__->load_namespaces;

1;
