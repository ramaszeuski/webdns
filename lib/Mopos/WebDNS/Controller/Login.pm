package Mopos::WebDNS::Controller::Login;
use Moose;
use namespace::autoclean;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

use constant REDIRECT_DEFAULT => '/zones/';
use constant REDIRECT_MAP     => [
    { role => 'root', url => '/customers/' },
];

=head1 NAME

Mopos::WebDNS::Controller::Login - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) :FormConfig('login.yaml') {}

sub index_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};

    my $login    = $form->param('login');
    my $password = $form->param('password');

    if ($c->authenticate(
        {
            login    => $login,
            password => $password,
            deleted  => undef,
        }
    )) {
        $c->model('DB::Log')->create(
            {
                event         => 'user_login',
                user_id       => $c->user->id,
                address       => $c->req->address,
            }
        );

        my $redirect = REDIRECT_DEFAULT;

        ROLES:
        foreach my $role ( @{ REDIRECT_MAP() } ) {
            if ($c->check_user_roles( $role->{role} )) {
                $redirect = $role->{url};
                last ROLES;
            }
        }

        $redirect ||= '/calls/';

        $c->response->redirect($c->uri_for($redirect));

        return;
    }
    else {
        $form->get_field('password')
            ->get_constraint({ type => 'Callback' })
            ->force_errors(1);
        $form->process;
    }

}

sub logout
:Global
:Args(0)
{
    my ( $self, $c ) = @_;
    $c->model('DB::Log')->create(
        {
            event         => 'user_logout',
            user_id       => $c->user->id,
            address       => $c->req->address,
        }
    );
    $c->delete_session('session expired');
    $c->logout;
    $c->response->redirect('/');
}
=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
