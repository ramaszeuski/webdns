package Mopos::WebDNS::Controller::Users;
use Moose;
use namespace::autoclean;

use utf8;
use Data::Password qw(:all);

$DICTIONARY = 5;
$GROUPS     = 2;
$MINLEN     = 6;
$MAXLEN     = 32;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

use constant BASE_URL => '/users/';

use constant FILTER_NAME   => 'filter_users';
use constant FILTER_FIELDS => {
    user        => {
        type => 'like',
        columns => [ qw(login firstname lastname) ],
    },
    customer_id => {},
    roles       => {},

};
=head1 NAME

Mopos::WebDNS::Controller::Users - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('users') :CaptureArgs(0) {
    my ($self, $c) = @_;
}

sub user :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $conditions = {
        id      => $id,
        deleted => undef,
    };

    if ( ! $c->stash->{is_root} ) {
        $conditions->{customer_id} = $c->user->customer_id;
    }

    my $user = $c->model('DB::User')->find( $conditions );

    if ( ! $c->stash->{is_root} ) {
        $user = undef if $user->roles =~ /root/;
    }

    die "User '$id' not found!" if ! $user;

    $c->stash(
        user  => $user,
        title => sprintf ( "%s (zákazník %s)",
            $user->login,
            $user->customer->name,
        )
    );
}

sub index :
PathPart('')
Args(0)
Menu('Uživatelé')
FormConfig('filter_users.yaml')
MenuOrder(75)
{
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    my $args = $c->request->params;

#    if ( $args->{customer_id} ) {
#        $c->stash->{customer_id} = $args->{customer_id};
#        $c->stash->{no_wrapper} = 1;
#    }

}

sub index_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form} // return;

    if ( ! $c->stash->{is_root} ) {
        $form->remove_element( $form->get_element( {id => 'customer_id'} ) );
    }

    $form->get_field( {name => 'roles'} )
        ->options(
            $c->model('DB::User')->roles({ allow_root => $c->stash->{is_root} })
        )
        ->value($c->session->{filter_users}{role})
    ;

    $c->stash->{ $self->FILTER_NAME } = $c->session->{ $self->FILTER_NAME }
        = $c->model('DB')->init_filter(
            {
                filter        => $c->session->{ $self->FILTER_NAME },
                filter_fields => $self->FILTER_FIELDS(),
                filter_form   => $form,
            }
        );

    $form->process();

}

sub json :Chained('base') :PathPart('json') :Args(0) {
    my ( $self, $c ) = @_;

    my $args  = $c->request->params;
    my $model = $c->model('DB::User_view');

    my ( $columns, $sorting, $conditions )
        = $c->model('DB')->datatables_args( $args );

    $conditions->{deleted} = undef;

    if ( ! $c->stash->{is_root} ) {
        $conditions->{customer_id} = $c->user->customer_id;
    }

    my $unfiltered_count = $model->count( $conditions, );

    my %filter = %{ $c->session->{$self->FILTER_NAME} // {} };

    my @where = $c->model('DB')->make_conditions_from_filter(
        {
            model         => $model,
            filter        => \%filter,
            filter_fields => $self->FILTER_FIELDS(),
        }
    );

    $conditions->{-and} = \@where if scalar @where;

    my $filtered_count = $model->count( $conditions, );

    my $records = $model->search(
        $conditions,
        {
            order_by => $sorting,
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},
        }
    );

    my @records = ();

    RECORD:
    while (my $record = $records->next) {

        my $editable = 0;
        $editable = 1 if $c->stash->{is_root};
        $editable = 1 if $c->stash->{is_admin} && $record->roles !~ /root/;

        push @records, {
            id            => $record->id,
            login         => $record->login,
            firstname     => $record->firstname,
            lastname      => $record->lastname,
            customer_name => $record->customer_name,
            customer_id   => $record->customer_id,
            role_names    => $record->role_names,
            DT_RowId      => $record->id,
            editable      => $editable,
       };
    }

    $c->stash(
        output => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@records,
            iTotalRecords        => $unfiltered_count,
            iTotalDisplayRecords => $filtered_count,
        },
    );

}

sub set_filter :Chained('base') :PathPart('set_filter') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    $c->session->{ $self->FILTER_NAME() } = {};

    for my $field (
            keys %{ $self->FILTER_FIELDS() }
        ) {

        if ( exists $args->{$field} && length $args->{$field} ) {
            $c->session->{$self->FILTER_NAME}{$field} = $args->{$field};
        }
        else {
            delete $c->session->{$self->FILTER_NAME}{$field};
        }
    }

    $c->response->body('OK');
}

sub add :
Chained('base')
PathPart('add')
Args(0)
Form
{
    my ( $self, $c ) = @_;

    if ( ! $c->check_any_user_role( qw(root admin) ) ) {
        $c->stash->{template} = 'denied.tt2';
        return;
    }

    if ( ! $c->stash->{is_root} && $c->stash->{licenses}{users}{free} <= 0 ) {
        $c->stash->{template} = 'users/license.tt2';
        return;
    }

    my $form = $c->stash->{form};

    $form->load_config_file('user.yaml');

    $form->remove_element(
        $form->get_field( { name => 'remove' } )
    );

    if ( ! $c->check_user_roles('root') ) {
        my $customer_id_field = $form->get_all_element( { name => 'customer_id' } );
        $customer_id_field->parent->remove_element( $customer_id_field  );
    }

    my $change_password = $form->get_all_element( { id => 'change_password' } );
    $change_password->parent->remove_element( $change_password );

    $form->get_field( {name => 'roles'} )
        ->options(
            $c->model('DB::User')->roles(
                { allow_root => $c->stash->{is_root}, }
            )
        )
    ;

    $c->stash->{form}->process;
}

sub add_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    my $args = $c->request->params;

#    if ( ref $args->{roles} eq 'ARRAY' ) {
#        $form->add_valid( roles => join ' ', @{ $args->{roles}} );
#    }

    if ( ! $c->stash->{is_root} ) {
        $form->add_valid( customer_id => $c->user->customer_id );
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    my $user = $form->model->create();

    $c->user->add_to_log_events(
        {
            event   => 'user_create',
            address => $c->req->address,
            data    => $user->id,
        }
    );

    $scope_guard->commit;
    $c->response->redirect( $c->uri_for(BASE_URL) );
    $c->detach;
}

sub edit :
Chained('user')
PathPart('')
Args(0)
Form
{
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form} // return;
    my $user = $c->stash->{user};

    $form->load_config_file('user.yaml');

    if ( ! $user->is_deletable || $user->id == $c->user->id ) {
        $c->stash->{form}->remove_element(
            $c->stash->{form}->get_field( { name => 'remove' } )
        );
    }

    if ( $c->check_user_roles('root') ) {
        my $login_field = $form->get_all_element( { name => 'login' } );
        $login_field->add_attrs( { readonly => 'readonly'} );
        my $password_field = $form->get_all_element( { name => 'password' } );
        $password_field->parent->remove_element( $password_field );
        if ( $user->roles =~ /root/ ) {
            $c->stash->{form}->remove_element(
                $c->stash->{form}->get_element( { id => 'credentials' } )
            );
#            my $change_password = $form->get_all_element( { id => 'change_password' } );
#            $change_password->parent->remove_element( $change_password );
        }
    }
    else {
        $c->stash->{form}->remove_element(
            $c->stash->{form}->get_element( { id => 'credentials' } )
        );
        my $customer_id_field = $form->get_all_element( { name => 'customer_id' } );
        $customer_id_field->parent->remove_element( $customer_id_field  );
    }

    $form->get_field( {name => 'roles'} )
        ->options(
            $c->model('DB::User')->roles(
                { allow_root => $c->check_user_roles( qw(root) ) }
            )
        )
    ;

    $form->model->default_values($user);

    $form->process;
}


sub edit_FORM_NOT_VALID {
}

sub edit_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    my $user = $c->stash->{user};
    my $args = $c->request->params;

    if ( ! $c->stash->{is_root} ) {
        $form->add_valid( customer_id => $c->user->customer_id );
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    if ( $args->{remove} ) {

        $user->update( { deleted => '\now()' } );

        $c->user->add_to_log_events(
            {
                event   => 'user_delete',
                address => $c->req->address,
                data    => $user->id,
            }
        );
    }
    else {

        $form->model->update( $user );

        $c->user->add_to_log_events(
            {
                event   => 'user_edit',
                address => $c->req->address,
                data    => $user->id,
            }
        );
    }

    $scope_guard->commit;

#    if (
#        $c->stash->{customer}
#        && $user->customer_id eq $c->stash->{customer}->id
#    ) {
        $c->response->redirect(BASE_URL);
#    }
#    else {
#        $c->flash->{rc} = 'saved_user';
#        $c->response->redirect(
#            $c->uri_for('/customers/' . $user->customer_id )
#        );
#    }

    $c->detach;
}


sub password :
Global
Args(0)
FormConfig('password.yaml')
{
    my ( $self, $c ) = @_;
}

sub password_FORM_VALID {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

        $c->user->update(
            {
                password => $args->{new_password},
            }
        );

        $c->user->add_to_log_events(
            {
                event   => 'change_password',
                address => $c->req->address,
            }
        );

    $scope_guard->commit;

    $c->stash->{saved} = 1;

}

sub change_password :
Chained('user')
PathPart('change_password')
Args(0)
{
    my ( $self, $c ) = @_;
    $c->stash->{output} = 'json';

    my $args = $c->request->params;
    my $user = $c->stash->{user};

    if ( ! $c->stash->{is_root} ) {
        $c->stash->{json} = { error => 'ACCESS DENIED' };
        return;
    }

    if ( IsBadPassword($args->{value}) ) {
        $c->stash->{json} = { error => 'Příliš jednoduché heslo' };
        return;
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

        $user->update(
            {
                password => $args->{value},
            }
        );

        $c->user->add_to_log_events(
            {
                event   => 'change_password',
                address => $c->req->address,
                data    => $user->id,
            }
        );

    $scope_guard->commit;

    $c->stash->{json} = { rc => 'OK' };
}

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
