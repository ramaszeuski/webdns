package Mopos::WebDNS::Controller::DNSSEC;
use Moose;
use namespace::autoclean;
use utf8;
use YAML;

BEGIN { extends 'Catalyst::Controller::HTML::FormFu'; }

=head1 NAME

Mopos::WebDNS::Controller::DNSSEC - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :
PathPart('')
Args(0)
Menu('DNSSEC')
MenuOrder(70)
MenuRoles('root')
{
    my ( $self, $c ) = @_;

}


sub ksk :Local :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{no_wrapper}  = 1;
}


sub keys :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;
    my @keys = ();

    KEY:
    foreach my $key (
        $c->model('DNSSEC')->get_keys(
            {
                type     => 'ksk',
                only_tld => 1,
            }
        )
    ) {
        delete $key->{rr}; # pbject nema co delat v json
        push @keys, {
           DT_RowId => $key->{id},
            %{ $key },
            activate => $key->{local_timestamp}{activate},
            delete   => $key->{local_timestamp}{delete} || '',
        }
    }

    $c->stash(
        output => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@keys,
            iTotalRecords        => scalar @keys,
            iTotalDisplayRecords => scalar @keys,
        },
    );

}

sub add_ksk :Local :Args(0) :FormConfig('ksk_add') {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};

    $c->stash->{no_wrapper}  = 1;

    my @algorithms = map {
        {
            value => $_->{id},
            label => $_->{id},

        }
    } $c->model('DNSSEC')->algorithms();

    my @tlds = map {
        {
            value => $_->{id},
            label => $_->{label},

        }
    } $c->model('DNSSEC')->tlds();

    $form->get_field( { id => 'algorithm' } )
        ->options( \@algorithms )
#        ->default(
#            $c->config->{dnssec}{default}{algorithm}
#        )
    ;

    $form->get_field( { id => 'tld' } )
        ->options( \@tlds )
    ;
}

sub add_ksk_save :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    $c->model('DNSSEC')->ksk_generate($args);

    $c->stash(
        output => 'json',
        json   => { rc => 'OK' },
    );
}

sub ksk_info :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    $c->stash(
        no_wrapper => 1,
        key        => $c->model('DNSSEC')->get_key($args->{id}),
    );
}

sub ksk_delete :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    my $key = $c->model('DNSSEC')->get_key(
        $args->{pk},
        { only_tld => 1 },
    );

    my $deletable = 1;

    if ( $args->{value} ) {
        # vyhledame jine klice pro zvoleny TLD
        my @keys = $c->model('DNSSEC')->get_keys(
            {
                name     => $key->{name},
                only_tld => 1,
            }
        );

        # v pripade ze klic je jediny - zkontrolujeme zony
        if ( scalar @keys == 1 ) {
            my $zones_conditions = {
                dnssec  => 't',
                deleted => undef,
            };

            if ( $key->{name} ne 'default' ) {
                $zones_conditions->{name} = { like => '%.' . $key->{name}};
            }


            my $zones_count = $c->model('DB::Zone')->count( $zones_conditions );

            if ( $zones_count ) {
                $deletable = 0;
            }
        }
    }

    if ( $deletable ) {
        $c->model('DNSSEC')->settime_key(
            $key,
            {
                timestamps => {
                    I => $args->{value},
                    D => $args->{value},
                }
            }
        );
        $c->stash(
            output => 'json',
            json   => { rc => 'OK' },
        );
    }
    else {
        $c->response->status(409);
    }
}

=encoding utf8

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
