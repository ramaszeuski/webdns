package Mopos::WebDNS::Controller::Ranges;
use Moose;
use namespace::autoclean;
use utf8;

use Net::IP;
use Data::Validate::Domain qw(is_domain);

use constant ERROR_MISSING_PLACEHOLDER => 'Šablona doménového jména musí obsahovat #';
use constant ERROR_INVALID_DOMAIN      => 'Neplatné doménové jméno %s';
use constant ERROR_PTR_EXIST           => 'Reverzní zaznam pro adresu %s již existuje';

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

sub base :Chained('/') :PathPart('ranges') :CaptureArgs(0) {
    my ($self, $c) = @_;

    my $args = $c->request->params;

    my $zone_id = $args->{zone_id} || $c->session->{zone_id};

    if ( $zone_id ) {
        my $zone = $c->model('DB::Zone')->find(
            {
                deleted => undef,
                id      => $zone_id,
            }
        );

        $c->stash->{zone}      = $zone;
        $c->session->{zone_id} = $zone->id;

    }

}

sub add :Chained('base') :PathPart('add') :Args(0) :Form {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    my $zone = $c->stash->{zone};

    $form->load_config_file('rr_PTR_range.yaml');

    $form->get_field( { id => 'zone_id' } )->default( $zone->id );

    my $ip = Net::IP->new( $zone->network );

    $form->get_field( { id => 'first' } )
         ->add_attributes( { prefix => $ip->short . '.' } );

    $form->get_field( { id => 'last' } )
         ->add_attributes( { prefix => $ip->short . '.' } );

    if ( $c->config->{default_ptr} ) {
        my @net = split /\D/, $ip->short;

        if ( $c->config->{default_ptr} =~ /^#/ ) {
            @net = reverse @net;
        }

        $c->stash->{default_ptr} = sprintf ($c->config->{default_ptr},  @net );

#        $form->get_field( { id => 'template' } )->default(
#        );
    }

    $c->stash->{default_ttl} = $c->config->{default_ttl} || $c->model('DB::RR')->DEFAULT_TTL();

    $form->get_field( { id => 'ttl' } )
        ->options(
            $c->model('DB::RR')->ttls( { label_name => 'label'} )
        )
#       ->default(
#            $c->model('DB::RR')->DEFAULT_TTL()
#       )
    ;


   $form->process;
   $c->stash->{no_wrapper} = 1;

}


#sub add_FORM_VALID {
sub add_save :Chained('base') :PathPart('add_save') :Args(0) { # add_FORM_VALID nefunguje
    my ( $self, $c ) = @_;

    my $args = $c->request->params;
    my $zone = $c->stash->{zone};

    $c->stash->{output} = 'json';

    my $zone_prefix = Net::IP->new( $zone->network )->short . '.';

    if ( $args->{template} !~ /#/) {
        $c->stash->{json} = {
            error => ERROR_MISSING_PLACEHOLDER,
            field => 'template',
        };
        return;
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    ADDRESS:
    foreach my $address ( $args->{first} .. $args->{last} ) {

        my $ip   = Net::IP->new( $zone_prefix . $address );
        my $name = $ip->reverse_ip;
        $name    =~ s/\.$//;

        my $ptr = $args->{template};
        $ptr =~ s/#/$address/;

        $c->log->debug( "$name >  $ptr" );

        if ( ! is_domain( $ptr ) ) {
            $c->stash->{json} = {
                error => sprintf(ERROR_INVALID_DOMAIN, $ptr),
                field => 'template',
            };
            return;
        }

        my $exists = $zone->rrs(
            {
                deleted => undef,
                name    => $name,
            }
        )->first;

        if ( $exists && $args->{collision} == 1 ) {
            $c->stash->{json} = {
                error => sprintf(ERROR_PTR_EXIST, $ip->ip),
                field => 'collision',
            };
            return;
        }

        if ( $exists && $args->{collision} == 2 ) {
            next ADDRESS;
        }

        if ( $exists && $args->{collision} == 4 ) {
            $exists->update( {
                customer_id => $args->{customer_id},
            } );
            next ADDRESS;
        }

        my $RR = Net::DNS::RR->new(
            type     => 'PTR',
            name     => $ip->reverse_ip,
            class    => 'IN',
            ttl      => $args->{ttl},
            ptrdname => $ptr,
        );

        if ( $exists ) {
            $exists->update( {
                ttl         => $RR->ttl,
                value       => $ptr,
                customer_id => $args->{customer_id},
            } );
        }
        else {
            $zone->add_to_rrs( {
                name        => $RR->name,
                type        => $RR->type,
                class       => $RR->class,
                ttl         => $RR->ttl,
                value       => $ptr,
                address     => $ip->ip,
                customer_id => $args->{customer_id},
            } );
        }

    }

    $scope_guard->commit;

    $c->user->add_to_log_events(
        {
            event   => 'range_create',
            address => $c->req->address,
            data    => $zone_prefix
                     . $args->{first}
                     . '-'
                     . $zone_prefix
                     . $args->{last},
        }
    );

    $c->model('RNDC')->update_zone( $zone );

    $c->stash->{json}   = { rc => 'OK' };
}

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
