package Mopos::WebDNS::Controller::Log;
use Moose;
use namespace::autoclean;
use utf8;

use Date::Manip;

Date_Init('DateFormat=non-US');

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

use constant FILTER_NAME   => 'filter_log';
use constant FILTER_FIELDS => {
    begin       => {
        type    => 'timestamp_begin',
        column  => 'timestamp',
#        default => UnixDate('today', '%d.%m.%Y'),
        no_save => 1,
    },
    end         => {
        type    => 'timestamp_end',
        column  => 'timestamp',
#        default => UnixDate('tomorrow', '%d.%m.%Y'),
        no_save => 1,
    },
    user        => {
        type => 'like',
        columns => [ qw(user_login user_firstname user_lastname) ],
    },
    event       => {},
    customer_id => {},
    address     => {},

};
=head1 NAME

Mopos::WebDNS::Controller::Log - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('log') :CaptureArgs(0) {
    my ($self, $c) = @_;
}

sub event :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $conditions = {
        id      => $id,
    };

    if ( ! $c->stash->{is_root} ) {
        $conditions->{customer_id} = $c->user->customer_id;
    }

    my $event = $c->model('DB::Log_view')->find( $conditions );

    die "Event '$id' not found!" if ! $event;

    $c->stash( event  => $event,  );
}

sub index :
PathPart('')
Args(0)
FormConfig('filter_log.yaml')
Menu('Záznam událostí')
MenuOrder(80)
#MenuRoles('root')
{
    my ( $self, $c ) = @_;

#    if ( ! $c->check_any_user_role( qw(root) ) ) {
#        $c->stash->{template} = 'denied.tt2';
#        return;
#    }
}

sub index_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form} // return;

    if ( ! $c->stash->{is_root} ) {
        $form->remove_element( $form->get_element( {id => 'customer_id'} ) );
    }

    $form->get_field( {name => 'event'} )
        ->options(
            $c->model('DB::Log_view')->events()
        )
        ->value($c->session->{filter_log}{event})
    ;

    $c->stash->{ $self->FILTER_NAME } = $c->session->{ $self->FILTER_NAME }
        = $c->model('DB')->init_filter(
            {
                filter        => $c->session->{ $self->FILTER_NAME },
                filter_fields => $self->FILTER_FIELDS(),
                filter_form   => $form,
            }
        );

    $form->process();

}

sub json :Chained('base') :PathPart('json') :Args(0) {
    my ( $self, $c ) = @_;

    my $args  = $c->request->params;
    my $model = $c->model('DB::Log_view');

    my ( $columns, $sorting, $conditions )
        = $c->model('DB')->datatables_args( $args );

    if ( ! $c->check_any_user_role( qw(root) ) ) {
        $conditions->{customer_id} = $c->stash->{customer}->id;
    }

    my $unfiltered_count = $model->count( $conditions, );

    my %filter = %{ $c->session->{$self->FILTER_NAME} // {} };

    my @where = $c->model('DB')->make_conditions_from_filter(
        {
            model         => $model,
            filter        => \%filter,
            filter_fields => $self->FILTER_FIELDS(),
        }
    );

    $conditions->{-and} = \@where if scalar @where;

    my $filtered_count = $model->count( $conditions, );

    my $records = $model->search(
        $conditions,
        {
            order_by => $sorting,
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},

        }
    );

    my @records = ();

    RECORD:
    while (my $record = $records->next) {
        push @records, {
            DT_RowId      => $record->id,
            event          => $record->event_name // $record->event,
            customer_name  => $record->customer_name,
            user_fullname  => $record->user_fullname,
            address        => $record->address,
            timestamp      => $record->timestamp,
            data           => $record->data,
        },
    }

    $c->stash(
        output => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@records,
            iTotalRecords        => $unfiltered_count,
            iTotalDisplayRecords => $filtered_count,
        },
    );

}

sub set_filter :Chained('base') :PathPart('set_filter') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    $c->session->{ $self->FILTER_NAME() } = {};

    for my $field (
            keys %{ $self->FILTER_FIELDS() }
        ) {

        if ( exists $args->{$field} && length $args->{$field} ) {
            $c->session->{$self->FILTER_NAME}{$field} = $args->{$field};
        }
        else {
            delete $c->session->{$self->FILTER_NAME}{$field};
        }
    }

    $c->response->body('OK');
}

sub detail :
Chained('event')
PathPart('')
Args(0)
Form
{
    my ( $self, $c ) = @_;
    my $event = $c->stash->{event};

    if ( $event->event =~ /user_(create|edit|delete)/ ) {
        $c->stash->{subject} = 'user';
        $c->stash->{user} = $c->model('DB::User_view')->find( { id => $event->data} );
    }

    if ( $event->event =~ /^customer/ ) {
        $c->stash->{subject} = 'user';
        $c->stash->{customer} = $c->model('DB::Customer')->find( { id => $event->data} );
    }

    if ( $event->event =~ /^rr/ ) {

        $c->stash->{subject} = 'rr';

        my $rrs = $c->model('DB::RR_view')->search(
            {
                id => { -in => [ split /\D/, $event->data ] }
            },
            {
                order_by => [ 'name' ]
            }
        );
        if ( $rrs->count ) {
            my @rrs = ();
            RRS:
            while ( my $rr = $rrs->next() ) {
                push @rrs, $rr;
                $c->stash->{zone} //= $rr->zone;
            }

            $c->stash->{rrs} = \@rrs;
        }
    }

    if ( $event->event =~ /^zone|dnssec/ ) {

        $c->stash->{subject} = 'zone';

        my $zones = $c->model('DB::Zone_view')->search(
            {
                id => { -in => [ split /\D/, $event->data ] }
            },
            {
                order_by => [ 'name' ]
            }
        );
        if ( $zones->count ) {
            my @zones = ();
            RRS:
            while ( my $zone = $zones->next() ) {
                push @zones, $zone;
            }

            $c->stash->{zones} = \@zones;
        }


    }

    $c->stash->{no_wrapper} = 1;
}

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
