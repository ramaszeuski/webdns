package Mopos::WebDNS::Controller::Customers;
use Moose;
use namespace::autoclean;

use utf8;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

use constant BASE_URL => '/customers/';

use constant TEMPLATE_LICENSES => [
    { feature_id => 'users',   granted => 1 },
    { feature_id => 'domains', granted => 1 },
];

=head1 NAME

Mopos::WebDNS::Controller::Customers - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut
sub base :Chained('/') :PathPart('customers') :CaptureArgs(0) {
    my ($self, $c) = @_;
}

sub customer :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $customer = $c->model('DB::Customer')->find( { id => $id } );

    $c->stash(
        customer => $customer,
    );

    die "Customer '$id' not found!" if ! $c->stash->{customer};
}

sub index :
PathPart('')
Args(0)
Menu('Zákazníci')
MenuOrder(10)
MenuRoles( 'root' )
{
    my ( $self, $c ) = @_;

    if ( ! $c->check_any_user_role( qw(root) ) ) {
        $c->stash->{template} = 'denied.tt2';
        return;
    }

    my $customers = $c->model('DB::Customer')->search(
        {
            deleted => undef
        },
        {
            order_by => ['name']
        }
    );

    my @customers = ();

    while ( my $customer = $customers->next ) {
        push @customers, $customer;
    }

    $c->stash->{customers} = \@customers;

}

sub selectbox :
Chained('base')
PathPart('selectbox')
Args(0)
{
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    if ( ! $c->check_any_user_role( qw(root) ) ) {
#        $c->stash->{template} = 'denied.tt2';
        return;
    }

    my $customers = $c->model('DB::Customer')->search(
        {
            deleted => undef
        },
        {
            order_by => ['name']
        }
    );

    my @customers = ();

    while ( my $customer = $customers->next ) {
        push @customers, $customer;
    }

    $c->stash(
        list        => $args->{list},
        datatable   => $args->{datatable},
        selected    => $c->session->{ 'filter_' .  $args->{list} }{customer_id},
        no_wrapper  => 1,
        customers   => \@customers,
    );

}

sub add :
Chained('base')
PathPart('add')
Args(0)
FormConfig('customer.yaml')
{

    my ( $self, $c ) = @_;

    if ( ! $c->check_any_user_role( qw(root) ) ) {
        $c->stash->{template} = 'denied.tt2';
        return;
    }

    my $form = $c->stash->{form};

}

sub add_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    $form->remove_element( $form->get_element( {id => 'remove'} ) );
}

sub add_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};
    my $args = $form->params;

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    my $customer = $form->model->create();

    LICENSE:
    foreach my $license ( @{ TEMPLATE_LICENSES() } ) {
        $customer->add_to_licenses( $license );
    }

    $customer->add_to_users(
        {
            login       => $args->{user_login},
            password    => $args->{user_password},
            roles       => 'admin',
        }
    );

    $c->user->add_to_log_events(
        {
            event   => 'customer_create',
            address => $c->req->address,
            data    => $customer->id,
        }
    );

    $scope_guard->commit;

    $c->response->redirect($c->uri_for(BASE_URL));
    $c->detach;
}

sub edit :Chained('customer') :PathPart('') :Args(0) :FormConfig('customer.yaml') {
    my ( $self, $c ) = @_;

    if ( ! $c->check_any_user_role( qw(root) ) ) {
        $c->stash->{template} = 'denied.tt2';
        return;
    }

    my $customer = $c->stash->{customer};

    my $form = $c->stash->{form};
    $form->auto_constraint_class( 'constraint_%t' );

    $form->remove_element( $form->get_element( {id => 'Admin'} ) );
}

sub edit_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form    = $c->stash->{form};
    my $customer = $c->stash->{customer};

    $form->model->default_values($customer);

    $c->stash->{title} = $customer->name;

}

sub json_select :Chained('base') :PathPart('json_select') :Args(0) {
    my ( $self, $c ) = @_;

    my $args     = $c->request->params;

    my $records = $c->model('DB::Customer')->search(
        {
            deleted => undef,
        },
        {
            order_by => 'name',
        }
    );

    my @records = ();

    RECORD:
    while (my $record = $records->next) {

        if ( $args->{simple} ) {
            push @records, $record->name;
        }
        else {
            push @records, {
                id   => $record->id,
                text => $record->name,
            };
        }
    }

    $c->stash(
        output => 'json',
        json    => \@records,
    );
}

sub edit_FORM_VALID {
    my ( $self, $c ) = @_;
    my $form     = $c->stash->{form};
    my $customer = $c->stash->{customer};
    my $args     = $c->request->params;

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    $c->model('DB::Log')->create(
        {
            event   => 'customer_edit',
            user_id => $c->user->id,
            address => $c->req->address,
            data    => $customer->id,
        }
    );

    $form->model->update($customer);

    if ( $args->{remove} ) {
        $customer->kill( { context => $c } );
    }

    $scope_guard->commit;

    $c->response->redirect($c->uri_for(BASE_URL));
    $c->detach;
}

sub licenses :Chained('customer') :PathPart('licenses') {
    my ( $self, $c ) = @_;

    my $customer = $c->stash->{customer};

    my $licenses = $customer->licenses_usage();

    my @licenses = ();

    FEATURE:
    foreach my $feature ( @{ $c->model('DB::License')->features() } ) {

        my $license = {
            feature_id   => $feature->{id},
            feature_name => $feature->{name},
            type         => $feature->{type},
        };

        if ( exists $licenses->{$feature->{id}} ) {
            $license->{granted} = $licenses->{$feature->{id}}{granted};
            $license->{used}    = $licenses->{$feature->{id}}{used};
            $license->{free}    = $licenses->{$feature->{id}}{free};
        }

        push @licenses, $license;
    }

    $c->stash(
        no_wrapper => 1,
        licenses   => \@licenses,
    );

}

sub save_license :Chained('customer') :PathPart('save_license') {
    my ( $self, $c ) = @_;

    my $customer = $c->stash->{customer};
    my $args    = $c->request->params;

    my $value   = $args->{value};

    $c->stash->{output} = 'json';

    # formalni validace
    if ( $value ) {
        if ( $value !~ /^\d+$/ ) {
            $c->stash->{json}{error} = "Neplatný počet $value";
            return;
        }
    }

    my $feature = $c->model('DB::License')->feature( $args->{feature_id} );

    if ( ! $feature ) {
        $c->stash->{json}{error} = "Neexistujici licence";
        return;
    }

    my $license = $customer->licenses(
        { feature_id => $args->{feature_id} }
    )->first;

    my $granted = $license ? $license->granted : 0;
    my $delta   = $value - $granted;

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    if ( $license ) {
        $license->update( { granted => $value } );
    }
    else {
        $license = $customer->add_to_licenses(
            {
                feature_id => $args->{feature_id},
                granted    => $value,
            }
        );
    }

    $scope_guard->commit;

    $c->stash->{json} = {
        free      => $license->free,
        used      => 0,
    };
}

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

__END__
sub users :
Chained('customer')
PathPart('users')
Args(0)
Form
{
    my ( $self, $c ) = @_;

    $c->stash->{no_wrapper} = 1;
}

sub add_user :
Chained('customer')
PathPart('add_user')
Args(0)
Form
{
    my ( $self, $c ) = @_;

    if ( ! $c->check_any_user_role( qw(customers) ) ) {
        $c->stash->{template} = 'denied.tt2';
        return;
    }

    my $customer = $c->stash->{customer};

    $c->stash->{title}    = sprintf ( "(customer %s)", $customer->name );
    $c->stash->{template} = 'users/add.tt2';
    $c->stash->{form}->load_config_file('user.yaml');

    $c->stash->{form}->remove_element(
        $c->stash->{form}->get_field( { name => 'remove' } )
    );

    $c->stash->{form}->process;
}

sub add_user_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form    = $c->stash->{form};
    my $args    = $c->request->params;
    my $customer = $c->stash->{customer};

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    my $user = $form->model->create();

    $c->model('DB::Log')->create(
        {
            event   => 'create_user',
            user_id => $c->user->id,
            address => $c->req->address,
            data    => $user->id,
        }
    );

    $scope_guard->commit;

    $c->flash->{rc} = 'saved_user';
    $c->response->redirect( $c->uri_for('/customers/' . $customer->id . '/edit/') );

    $c->detach;
}

