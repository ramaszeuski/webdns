package Mopos::WebDNS::Controller::Zones;
use Moose;
use namespace::autoclean;

use utf8;
use Date::Manip;
use Net::DNS;
use YAML;

BEGIN { extends 'Catalyst::Controller::HTML::FormFu'; }

use constant BASE_URL     => '/zones/';

use constant FILTER_NAME   => 'filter_zones';
use constant FILTER_FIELDS => {
    fulltext        => {
        type    => 'ilike',
        columns => [ qw(name network_text notes) ],
    },
    notes          => { type    => 'ilike', },
    customer_name  => { type    => 'ilike', },
    name           => { type    => 'ilike', },
    network_text   => { type    => 'like', },
    customer_id    => {},

};
=head1 NAME

Mopos::WebDNS::Controller::Zones - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('zones') :CaptureArgs(0) {
    my ($self, $c) = @_;
}

sub zone :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $conditions = {
        id      => $id,
        deleted => undef,
    };

    if ( ! $c->stash->{is_root} ) {
        $conditions->{customer_id} = $c->user->customer_id;
    }

    my $zone = $c->model('DB::Zone')->find( $conditions );

    die "Zone '$id' not found!" if ! $zone;

    my $title = $zone->name;

    if ( $zone->type == 3 || $zone->type == 4 ) {
        $title .= ' (síť ' . $zone->network . ')';
    }

    $c->stash(
        title => $title,
        zone  => $zone,
    );

}

=head2 index

=cut

sub index :
PathPart('')
Args(0)
Menu('Domény')
MenuOrder(20)
{
    my ( $self, $c ) = @_;
}

sub list :
Chained('base')
PathPart('list')
Args(0)
{
    my ( $self, $c ) = @_;
    my $args = $c->request->params;

    $c->stash->{type}        = $args->{type};
    $c->session->{zone_type} = $args->{type};
    $c->stash->{no_wrapper} = 1;

}

sub json :Chained('base') :PathPart('json') :Args(0) {
    my ( $self, $c ) = @_;

    my $args  = $c->request->params;
    my $model = $c->model('DB::Zone_view');

    my ( $columns, $sorting ) = $c->model('DB')->datatables_args( $args );
    my $conditions = {};

    $conditions->{deleted} = undef;
    $conditions->{type}    = $args->{type};

    if ( ! $c->stash->{is_root} ) {
        $conditions->{customer_id} = $c->user->customer_id;
    }

    my $unfiltered_count = $model->count( $conditions, );

    my %filter = %{ $c->session->{ $self->FILTER_NAME } // {} };
    $filter{fulltext} = $args->{sSearch} if $args->{sSearch};

    my @where = $c->model('DB')->make_conditions_from_filter(
        {
            model         => $model,
            filter        => \%filter,
            filter_fields => $self->FILTER_FIELDS(),
        }
    );

    $conditions->{-and} = \@where if scalar @where;

    my $filtered_count = $model->count( $conditions, );

    my $records = $model->search(
        $conditions,
        {
            order_by => $sorting,
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},
        }
    );

    my @records = ();

    RECORD:
    while (my $record = $records->next) {

        push @records, {
            id            => $record->id,
            dnssec        => $record->dnssec,
            active        => $record->active,
            name          => $record->name,
            name_reverse  => $record->name,
            network       => $record->network,
            customer_name => $record->customer_name,
            masters       => $record->masters,
            state         => $record->state,
            notes         => $record->notes || '',
            DT_RowClass   => 'Z' . $record->state,
       };
    }

    $c->stash(
        output => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@records,
            iTotalRecords        => $unfiltered_count,
            iTotalDisplayRecords => $filtered_count,
        },
    );

}

sub add :
Chained('base')
PathPart('add')
Form
Args(0)
{
    my ( $self, $c ) = @_;

    my $args = $c->request->params;
    my $form = $c->stash->{form} // return;

    $form->load_config_file('zone_add_' . $args->{type}  . '.yaml');

    if ( ! $c->stash->{is_root} && $c->stash->{licenses}{zones}{free} <= 0 ) {
        $c->stash->{template} = 'zones/license.tt2';
        return;
    }

    my $customer_field = $form->get_element( {id => 'customer_id'} );
    if ( $c->stash->{is_root} ) {
        $customer_field->default( $c->user->customer_id );
    }
    else {
        $form->remove_element( $customer_field );
    }

    $form->process();
}

sub add_FORM_VALID {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;
    my $form = $c->stash->{form} // return;

    my %options = ();

    if ( ! $c->stash->{is_root} ) {
        $form->add_valid( customer_id => $c->user->customer_id );
    }

    my $name;
    if ( $args->{type} == 3 || $args->{type} == 4 ) {
        my $ip = Net::IP->new( $args->{network} );
        $name = $ip->reverse_ip();
    }
    else {
        $name = lc( $args->{name} );
    }

    $name =~ s/\s+$//;
    $name =~ s/^\s+//;
    $name =~ s/\.$//; #TODO: nejspis zbytecny

    $form->add_valid( active => 'f' );
    $form->add_valid( name   => $name );
    $form->add_valid( type   => $args->{type} );

    # master servery pro slave zonu
    if ( $args->{type} == 2 ) {
        my @masters = split /[^\w\.]+/, $args->{masters};
        $form->add_valid( masters => join ';', @masters );
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    my $zone = $form->model->create();

    # primarni zona - nacist defaultni RR

    if ( $args->{type} == 1 or $args->{type} == 3  or $args->{type} == 4 ) {
        RR:
        foreach my $rr ( @{ $c->config->{default}{rrs} } ) {
            # BUG: modifikuje $c->config->{default}{rrs} !!!
            # $rr->{name} = join '.', grep /./, $rr->{name}, $zone->name;
            $zone->add_to_rrs(
                {
                  %{ $rr },
                  state => 2,
                  name  => join ('.', grep /./, $rr->{name}, $zone->name),
                }
            );
        }
    }

    $c->user->add_to_log_events(
        {
            event   => 'zone_create',
            address => $c->req->address,
            data    => $zone->id,
        }
    );

    $c->model('RNDC')->make_zone_file( $zone, { context => $c } );

    $scope_guard->commit;

    $c->flash->{type} = $zone->type;
    $c->response->redirect( $c->uri_for(BASE_URL) );
    $c->detach;

}

sub edit :Chained('zone') :PathPart('') :Args(0) :FormConfig('zone.yaml') {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;
    my $form = $c->stash->{form} // return;
    my $zone = $c->stash->{zone} // return;

    $c->stash->{editable} = 1;

    if ( ! $c->stash->{is_root} ) {
        $form->remove_element( $form->get_element( {id => 'customer_id'} ) );
        $c->stash->{editable} = 0 if $zone->type == 4;
    }
    else {
        # prozatim jen root
        if ( $zone->type == 1 && $zone->name =~ /cz$/ ) {
            $c->stash->{dnssec_allowed} = 1;
        }
    }

    if ( $zone->type != 2 ) {
        $form->remove_element( $form->get_element( {id => 'masters'} ) );
    }

    # v nekterych pripadech neni co editovat

    $form->model->default_values( $zone );

    $form->process();
}

sub edit_FORM_NOT_VALID {
    my ( $self, $c ) = @_;
    $c->flash->{rc} = 'saved';
}

sub edit_FORM_VALID {
    my ( $self, $c ) = @_;
    my $args = $c->request->params;

    my $form = $c->stash->{form} // return;
    my $zone = $c->stash->{zone} // return;

    my %options = ();
    my $changed = 0;
    my $dnssec  = undef;

    $changed = 1 if $args->{allow_transfer} ne $zone->allow_transfer;
    my @allow_transfer = split /[\s\,\;]+/, $args->{allow_transfer};
    $form->add_valid( allow_transfer => join ';', @allow_transfer);

    # master servery pro slave zonu
    if ( $zone->type == 2 ) {
        $changed = 1 if $args->{masters} ne $zone->masters;
        my @masters = split /[^\w\.]+/, $args->{masters};
        $form->add_valid( masters => join ';', @masters);
    }

    # rozhodovani o dnssec
    $dnssec = 1 if $args->{dnssec} && ! $zone->dnssec;
    $dnssec = 0 if $zone->dnssec && ! $args->{dnssec};

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

        $c->model('RNDC')->delete_zone( $zone, $c ) if $changed;

        $form->model->update( $zone );

        if ( scalar keys %options ) {
            $zone->update( { options => \%options } );
        }

        $c->model('RNDC')->add_zone( $zone, $c ) if $changed;

        $c->user->add_to_log_events(
            {
                event   => 'zone_update',
                address => $c->req->address,
                data    => $zone->id,
            }
        );

    $scope_guard->commit;

# $c->flash->{type} = $zone->type;
# $c->response->redirect( $c->uri_for(BASE_URL) );

    $c->flash->{rc} = 'saved';
    $c->response->redirect( $c->uri_for(BASE_URL) . $zone->id . '/' );
    $c->detach;
}

sub dnssec :Chained('zone') :PathPart('dnssec') :Args(0) {
    my ( $self, $c ) = @_;
    my $args = $c->request->params;

    my $zone = $c->stash->{zone} // return;

    # neni zmena - ihned ukoncime
    if ( $zone->dnssec == $args->{status} ) {
        $c->stash(
            output => 'json',
            json    => { status => $zone->dnssec },
        );
        return;
    }

    # ziskame nadrazenou zonu pro pripadne pridani DS
    my $local_parent = $zone->local_parent;

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

        $zone->update( {
            dnssec => $args->{status},
        });

        if ( $zone->dnssec ) {
            $c->model('DNSSEC')->zsk_generate( $zone );
            $c->model('DNSSEC')->ksk_clone( $zone );

            $c->model('RNDC')->reconfigure_zone( $zone, { context => $c } );

            if ( $local_parent ) {

                my @dsset = $c->model('DNSSEC')->dsset( $zone );

                #TODO: pouzito i v script/dnssec_keys - unifikovat
                RR:
                foreach my $rr ( @dsset ) {
                    next RR if $rr->digtype != 2;
                    $local_parent->add_to_rrs(
                        {
                            state => 2,
                            name  => $zone->name,
                            type  => 'DS',
                            class => 'IN',
                            ttl   => $c->config->{dnssec}{ttl}{ds},
                            value => join (' ',
                                $rr->keytag,
                                $rr->algorithm,
                                $rr->digtype,
                                $rr->digest,
                            ),
                        }
                    );
                }
                $c->model('RNDC')->update_zone( $local_parent );
            }
        }
        else {
            # neumoznit vypnuti v pripade existence ds
            # pred vypnutim vyzadat potvrzeni stejne jaku u mazani zony

            # navysit serial z dynamickeho zonoveho souboru!
            my $serial = $c->model('RNDC')->get_serial( $zone );
            $zone->update_serial( ++$serial );
            # a vygenerovat konfigurak bez inline-signing
            $c->model('RNDC')->reconfigure_zone( $zone, { context => $c, wipe => 1 } );
            # smazat klonovane ksk
            $c->model('DNSSEC')->delete_keys( $zone, { type => 'ksk' } );

            if ( $local_parent ) {
                # TODO: mozna jen zmena "deleted"?
                $local_parent->rrs(
                    {
                        name  => $zone->name,
                        type  => 'DS',
                    }
                )->delete;
                $c->model('RNDC')->update_zone( $local_parent );
            }
        }

        $c->user->add_to_log_events({
            event   => ($zone->dnssec ? 'dnssec_enabled' : 'dnssec_disabled'),
            address => $c->req->address,
            data    => $zone->id,
        });

    $scope_guard->commit;

    # tady zapnout dnssec

    $c->stash(
        output => 'json',
        json    => {
            dnssec => $zone->dnssec,
        },
    );
}

sub master :Chained('base') :PathPart('master') :Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{output} = 'json';

    my $args = $c->request->params;

    my $conditions = {
        id      => { -in => [split /\D+/, $args->{id}] },
        deleted => undef,
    };

    if ( ! $c->stash->{is_root} ) {
        $conditions->{customer_id} = $c->user->customer_id;
    }

    my $zones = $c->model('DB::Zone')->search( $conditions );

    ZONE:
    while ( my $zone = $zones->next() ) {
        $zone->update_secondary( { context => $c } );
        $zone->update( { type => 1 });
        $c->model('RNDC')->add_zone( $zone, $c );
    }

    $c->stash->{json}   = { rc => 'OK' };
}

sub export :Chained('base') :PathPart('export') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    my $conditions = {
        id      => { -in => [split /\D+/, $args->{id}] },
        deleted => undef,
    };

    if ( ! $c->stash->{is_root} ) {
        $conditions->{customer_id} = $c->user->customer_id;
    }

    my $zones = $c->model('DB::Zone')->search( $conditions );

    my $archive = $c->model('RNDC')->zip( $zones, $c );

    $c->response->header('Content-Disposition', 'attachment; filename="zones.zip"');
    $c->response->content_type( 'application/zip' );
    $c->serve_static_file( $archive );
    unlink $archive;

}

sub activate :Chained('base') :PathPart('activate') :Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{output} = 'json';

    return if ! $c->stash->{is_root};

    my $args      = $c->request->params;
    my @ids       = split /\D+/, $args->{id};
    my @zones     = ();
    my %activated = ();

    my $conditions = {
        id      => { -in => \@ids },
        deleted => undef,
        active  => 'f',
    };

    my $zones = $c->model('DB::Zone')->search( $conditions );

    ZONE:
    while ( my $zone = $zones->next() ) {
        my $activated;

        if ( $activated{ $zone->name }++ ) {
            $activated = 1;
        }
        else {
            $activated = $c->model('DB::Zone')->count(
                {
                    name    => $zone->name,
                    deleted => undef,
                    active  => 't',
                }
            );
        }

        if ( $activated ) {
            $c->stash->{json}   = {
                error => sprintf (
                    "Nemůžete současně aktivovat více než jednu doménu se stejnym jmeném (%s)",
                    $zone->name
                )
            };
            return;
        }
        else {
            push @zones, $zone;
        }
    }

    ZONE_ACTIVATE:
    foreach my $zone ( @zones ) {
        my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

            $zone->update( { active => 1 });
            $c->model('RNDC')->add_zone( $zone, $c );

            $c->user->add_to_log_events(
                {
                    event   => 'zone_activate',
                    address => $c->req->address,
                    data    => $zone->id,
                }
            );

        $scope_guard->commit;

    }

    $c->stash->{json}   = { rc => 'OK' };
}

sub deactivate :Chained('base') :PathPart('deactivate') :Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{output} = 'json';

    return if ! $c->stash->{is_root};

    my $args = $c->request->params;

    my $conditions = {
        id      => { -in => [split /\D+/, $args->{id}] },
        deleted => undef,
        active  => 't',
    };

    my $zones = $c->model('DB::Zone')->search( $conditions );

    ZONE:
    while ( my $zone = $zones->next() ) {
        my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

            $zone->update( { active => 0 });
            $c->model('RNDC')->deactivate_zone( $zone, $c );

            $c->user->add_to_log_events(
                {
                    event   => 'zone_deactivate',
                    address => $c->req->address,
                    data    => $zone->id,
                }
            );

        $scope_guard->commit;
    }

    $c->stash->{json}   = { rc => 'OK' };
}

sub raw :Chained('zone') :PathPart('raw') :Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{no_wrapper} = 1;
}

sub dnssec_info :Chained('zone') :PathPart('dnssec_info') :Args(0) {
    my ( $self, $c ) = @_;

    my $zone  = $c->stash->{zone} // return;
    my @dsset = $c->model('DNSSEC')->dsset( $zone );

    my $local_parent = $zone->local_parent();

    $c->stash->{dnssec}    = $zone->dnssec;
    $c->stash->{dsset}     = [ map { $_->plain } @dsset ];
    $c->stash->{keysets}   = $c->model('DNSSEC')->keysets( $zone );
    $c->stash->{editable } = 1;

    # v pripade ze zona ma nekde platny DS - zakazeme vypnuti dnssec
    if ( $zone->dnssec && ! $local_parent ) { #|| $c->stash->{is_root}
        # a co kdyz je parent u nas???

        my %dsset = map { $_->digest => 1 } @dsset;

        my $resolver = new Net::DNS::Resolver();

        my $reply = $resolver->query( $zone->name_idn, 'DS' );
        if ($reply) {
            my @answer = $reply->answer();
            RR:
            foreach my $rr ( @answer ) {
                if ( $dsset{$rr->digest} ) {
                    $c->stash->{editable} = 0;
                    last RR;
                }
            }
        }
    }


    $c->stash->{no_wrapper} = 1;
}


sub save_field :Chained('base') :PathPart('save') :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{output} = 'json';
    my $args = $c->request->params;

    my $conditions = {
        id      => $args->{pk},
        deleted => undef,
    };

    my $zone = $c->model('DB::Zone')->find( $conditions );

    if ( ! $c->stash->{is_root} ) {
        $zone = undef if $zone->customer_id != $c->user->customer_id;
    }

    if ( ! $zone ) { # pokus editovat neexistujici nebo cizi zaznam
        $c->response->code(400);
        return;
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    $zone->update( {
        $args->{name} => $args->{value},
    } );

    my $log = $c->user->add_to_log_events(
        {
            event   => 'zone_edit',
            address => $c->req->address,
            data    => $zone->id,
        }
    );

    $scope_guard->commit;

    $c->stash->{json} = { rc => 'OK' };
}

sub delete :Chained('base') :PathPart('delete') :Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{output} = 'json';

    my $args = $c->request->params;
    my @ids  = split /\D+/, $args->{id};

    my $conditions = {
        id      => { -in => \@ids },
        deleted => undef,
        active  => 't',
    };

    if ( $c->model('DB::Zone')->count( $conditions )) {
        $c->response->code(400);
        $c->response->body("Nemůžete mazat aktivni domény");
        return;
    }

    ZONE:
    foreach my $id ( @ids ) {

        my $conditions = {
            id      => $id,
            deleted => undef,
            active  => 0,
        };

        if ( ! $c->stash->{is_root} ) {
            $conditions->{customer_id} = $c->user->customer_id;
        }

        my $zone = $c->model('DB::Zone')->search( $conditions )->first;

        next ZONE if ! $zone;

        my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

            $c->model('RNDC')->delete_zone( $zone );
            $zone->kill( { context => $c } );

        $scope_guard->commit;

    }

    $c->stash->{json}   = { rc => 'OK' };
}

sub set_filter_field :Chained('base') :PathPart('set_filter_field') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    if ( length $args->{value} ) {
        $c->session->{ FILTER_NAME() }{ $args->{field} } = $args->{value};
    }
    else {
        delete $c->session->{ FILTER_NAME() }{ $args->{field} };
    }

    $c->response->body('OK');
}


=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

__END__
