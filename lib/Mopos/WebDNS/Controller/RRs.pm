package Mopos::WebDNS::Controller::RRs;
use Moose;
use namespace::autoclean;
use utf8;

use Net::IP;
use YAML;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

use constant FILTER_NAME   => 'filter_rrs';
use constant FILTER_FIELDS => {
    fulltext  => {
        type    => 'ilike',
        columns => [ qw(name address_text value) ],
    },
    customer_id => { },
};
=head1 NAME

Mopos::WebDNS::Controller::RRs - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('rrs') :CaptureArgs(0) {
    my ($self, $c) = @_;

    my $args = $c->request->params;

    my $zone_id = $args->{zone_id} || $c->session->{zone_id};


    if ( $zone_id ) {
        my $zone = $c->model('DB::Zone')->find(
            {
                deleted => undef,
                id      => $zone_id,
            }
        );

        $c->stash->{zone}      = $zone;
        $c->session->{zone_id} = $zone->id;

    }

}

sub rr :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $conditions = {
        id => $id,
    };

#    if ( ! $c->stash->{is_root} ) {
#        $conditions->{customer_id} = $c->user->customer_id;
#    }

    my $event = $c->model('DB::RR_view')->find( $conditions );

    die "RR '$id' not found!" if ! $event;

    $c->stash( rr  => $event,  );
}

sub index :Chained('base') :PathPart('') :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{no_wrapper}  = 1;

    my $zone = $c->stash->{zone} // return;

    $c->session->{zone_type} = $zone->type;


    if ( $zone->type == 2 ) {
        if ( $zone->state == 1 ) {
            $c->stash->{template} = 'zones/updating.tt2';
            return;
        }
        else {
            $zone->update_secondary( { context => $c } );
        }
    }

    if ( $zone->type == 3 ) { # nactemne seznam zakazniku pro moznost zmeny
        my @customers = ();
        my $customers = $c->model('DB::Customer')->search(
            {
                active  => 1,
                deleted => undef,
            },
            {
                order_by => ['name']
            }
        );

        CUSTOMER:
        while ( my $customer = $customers->next ) {
            push @customers, {
                id   => $customer->id,
                name => $customer->name,
            }
        }

        $c->stash->{customers} = \@customers;

    }

    $c->stash->{template}    = 'rrs/index_' . $zone->type . '.tt2';
}

sub json :Chained('base') :PathPart('json') :Args(0) {
    my ( $self, $c ) = @_;

    my $args  = $c->request->params;
    my $model = $c->model('DB::RR_view');

    my ( $columns, $sorting ) = $c->model('DB')->datatables_args( $args );
    my $conditions = {};

    if ( $args->{reverse} ) {
        $conditions->{type}         = 'PTR';
        $conditions->{zone_type}    = 3;
        $conditions->{zone_deleted} = undef;
        if ( ! $c->stash->{is_root} ) {
            $conditions->{customer_id}  = $c->user->customer_id;
        }
    }
    else {
        my $zone = $c->stash->{zone} // return;
        $conditions->{zone_id} = $zone->id;
        $conditions->{type}    = { '!=' => 'SOA' };
    }

    $conditions->{deleted} = undef;

    my $unfiltered_count = $model->count( $conditions, );

    my %filter = %{ $c->session->{ FILTER_NAME() } // {} };
    delete $filter{customer_id} if ! $args->{reverse};

    $filter{fulltext} = $args->{sSearch} if $args->{sSearch};

    my @where = $c->model('DB')->make_conditions_from_filter(
        {
            model         => $model,
            filter        => \%filter,
            filter_fields => $self->FILTER_FIELDS(),
        }
    );

    $conditions->{-and} = \@where if scalar @where;

    my $filtered_count = $model->count( $conditions, );

    my $records = $model->search(
        $conditions,
        {
            order_by => $sorting,
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},
        }
    );

    my @records = ();

    RECORD:
    while ( my $record = $records->next ) {

        push @records, {
            DT_RowId      => $record->id,
            id            => $record->id,
            name          => $record->name,
            name_reverse  => $record->name,
            type          => $record->type,
            type_priotity => $record->type,
            ttl           => $record->ttl,
            address       => $record->address,
            customer_name => $record->customer_name,
            preference    => $record->preference,
            weight        => $record->weight,
            port          => $record->port,
            value         => $record->value,
            state         => $record->state,
       };
    }

    $c->stash(
        output => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@records,
            iTotalRecords        => $unfiltered_count,
            iTotalDisplayRecords => $filtered_count,
        },
    );

}

sub add :Chained('base') :PathPart('add') :Args(0) :Form {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;
    my $type = $c->stash->{type} = $args->{type};
    my $form = $c->stash->{form};
    my $zone = $c->stash->{zone};

    $form->load_config_file('rr_' . $type  . '.yaml');

    $form->get_field( { id => 'type'    } )->default( $type );
    $form->get_field( { id => 'zone_id' } )->default( $zone->id );

    my $name_field = $form->get_field( { id => 'name' } );

    if ( $zone->type == 3 && $type =~ /PTR/ ) {
        my $ip = Net::IP->new( $zone->network );
        $name_field->add_attributes( { prefix => $ip->short . '.' } );
    }
    elsif ( $zone->type == 4 && $type eq 'PTR' ) {
        $name_field->default($zone->prefix_ipv6);
#        $name_field->add_attributes( { prefix => $zone->prefix_ipv6 } );
    }
    else {
        $name_field->comment( '.' . $zone->name );
    }

    $c->stash->{default_ttl} = $c->config->{default_ttl} || $c->model('DB::RR')->DEFAULT_TTL();

    $form->get_field( { id => 'ttl' } )
        ->options(
            $c->model('DB::RR')->ttls( { label_name => 'label'} )
        )
       ->default(
            $c->model('DB::RR')->DEFAULT_TTL()
        )
    ;

    $c->stash->{title}  = $type;
    $c->stash->{no_wrapper} = 1;

}

sub add_save :Chained('base') :PathPart('add_save') :Args(0) { # add_FORM_VALID nefunguje
    my ( $self, $c ) = @_;

    my $args  = $c->request->params;
    my $zone  = $c->stash->{zone};
    my $state = 0;

    $c->stash->{output} = 'json';

    # limit maximalniho poctu rrv domene
    my $rr_count = $zone->rrs(
        { deleted => undef }
    )->count();

    if ( $rr_count >= $zone->max_rr ) {
        $c->stash->{json} = {
            error => 'Překročen limit počtů zaznámů v doméně',
            field => 'name',
        };
        return;
    }

    $args->{zone} = $zone;

    # u nekterych zaznamu nekdy doplnime nazev domeny
    if ( $args->{type} =~/CNAME|MX|NS|SRV/ && $args->{value} !~ /\./ ) {
        $args->{value} .= '.' . $zone->name;
    }

    # predevsim kvuli validaci
    my ( $RR, $options ) = $c->model('DB::RR')->construct( $args );

    if ( ref $RR eq 'HASH' && $RR->{error} ) {
        $c->log->debug(Dump $RR);
        $c->stash->{json} = $RR;
        return;
    }

    # kontrola kolize CNAME
    if ( $RR->type eq 'CNAME' ) {
        my $exists = $zone->rrs(
            {
                deleted => undef,
                name    => $RR->name,
            }
        )->count;
        if ( $exists ) {
            $c->stash->{json} = {
                error => 'CNAME s tímto jménem nelze přidat,<br/> protože na toto jméno již existuje jiný záznam',
                field => 'name',
            };
            return;
        }
    }

    # kontrola pridani DS pri neexistujicim NS
    if ( $RR->type eq 'DS' ) {
        my $exists = $zone->rrs(
            {
                deleted => undef,
                type    => 'NS',
                name    => $RR->name,
            }
        )->count();
        if ( ! $exists ) {
            $c->stash->{json} = {
                error => 'DS s tímto jménem nelze přidat,<br/> protože neexistuje NS záznam',
                field => 'name',
            };
            return;
        }
    }

    # vpripade DS zakazujeme editaci a do value skladame vsechno
    if ( $RR->type eq 'DS' ) {
        $state = 2;
        $args->{value} = join ' ', (
            $args->{keytag},
            $args->{algorithm},
            $args->{digtype},
            $args->{value},
        );
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    my $rr = $zone->add_to_rrs( {
        state       => $state,
        name        => $RR->name,
        type        => $args->{type},
        ttl         => $args->{ttl},
        preference  => $args->{preference},
        weight      => $args->{weight},
        port        => $args->{port},
        value       => $args->{value},

        class       => $RR->class,

        address     => $options->{address},

    } );

    my $log = $c->user->add_to_log_events(
        {
            event   => 'rr_create',
            address => $c->req->address,
            data    => $rr->id,
        }
    );

    my $history = $rr->history(
        {
            log_id  => undef,
        },
        {
            order_by => { -desc => 'changed' },
        }
    )->first;

    $history->update(
        { log_id => $log->id }
    );

    $c->model('RNDC')->update_zone( $zone );

    $scope_guard->commit;

    $c->stash->{json}   = { rc => 'OK' };
}

sub save_field :Chained('base') :PathPart('save') :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{output} = 'json';
    my $args = $c->request->params;

    my $conditions = {
        id      => $args->{pk},
        deleted => undef,
    };

    my $rr = $c->model('DB::RR')->find( $conditions );

    if ( ! $c->stash->{is_root} ) {
        if ( $rr->type eq 'PTR' && $rr->zone->type == 3 ) {
            $rr = undef if $rr->customer_id != $c->user->customer_id;
        }
        else {
            $rr = undef if $rr->zone->customer_id != $c->user->customer_id;
        }
    }

    if ( ! $rr ) { # pokus editovat neexistujici nebo cizi zaznam
        $c->response->code(400);
        return;
    }

    my ( $RR ) = $c->model('DB::RR')->construct(
        {
            rr  => $rr,
            $args->{name} => $args->{value},
        }
    );

    if ( ref $RR eq 'HASH' && $RR->{error} ) {
        $c->stash->{json} = $RR;
        return;
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    $rr->update( {
        ttl           => $RR->ttl,
        $args->{name} => $args->{value},
    } );

    my $log = $c->user->add_to_log_events(
        {
            event   => 'rr_edit',
            address => $c->req->address,
            data    => $rr->id,
        }
    );

    my $history = $rr->history(
        {
            log_id => undef,
        },
        {
            order_by => { -desc => 'changed' },
        }
    )->first;

    $history->update(
        { log_id => $log->id }
    );

    $c->model('RNDC')->update_zone( $rr->zone, { context => $c } );

    $scope_guard->commit;

    $c->stash->{json} = { rc => 'OK' };
}

sub delete :Chained('base') :PathPart('delete') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    my @ids     = split /\D+/, $args->{id};
    my @deleted = ();
    my $zone    = undef;

    my $conditions = {
        'me.id'      => { -in => \@ids },
        'me.deleted' => undef,
    };

    if ( ! $c->stash->{is_root} ) {
        $conditions->{'zone.customer_id'} = $c->user->customer_id;
    }

    my $rrs = $c->model('DB::RR')->search(
        $conditions,
        {
            order_by => 'type', # poradi mavyznam kvuli primarnimu mazani DS
            join     => 'zone',
        }
    );

    return if ! $rrs->count();

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    RR:
    while ( my $rr = $rrs->next ) {
        $zone //= $rr->zone;

        # nemazat NS v pripade ze existuje DS
        if ( $rr->type eq 'NS' ) {
            my $ds = $c->model('DB::RR')->count(
                {
                    name    => $rr->name,
                    type    => 'DS',
                    deleted => undef
                }
            );
            if ( $ds ) {
                #TODO: nejaka chybova hlaska?
                next RR;
            }
        }

        $rr->update( { deleted => \'now()' } );
        push @deleted, $rr->id;
    }

    $c->user->add_to_log_events(
        {
            event   => 'rr_delete',
            address => $c->req->address,
            data    => join (',', @deleted),
        }
    );

    $c->model('RNDC')->update_zone( $zone );

    $scope_guard->commit;

    $c->stash->{output} = 'json';
    $c->stash->{json}   = { rc => 'OK' };
}

sub owner :Chained('base') :PathPart('owner') :Args(0) {
    my ( $self, $c ) = @_;

    return if ! $c->stash->{is_root};

    my $args = $c->request->params;

    my @ids = split /\D+/, $args->{id};


    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

        $c->model('DB::RR')->search(
            {
                id   => { -in => \@ids },
                type => 'PTR',
            }
        )->update(
            {
                customer_id => $args->{customer_id}
            }
        );

        $c->user->add_to_log_events(
            {
                event   => 'rr_change_owner',
                address => $c->req->address,
                data    => join (',', @ids),
            }
        );

    $scope_guard->commit;

    $c->stash->{output} = 'json';
    $c->stash->{json}   = { rc => 'OK' };
}

sub ptr4 :
Chained('base')
PathPart('ptr4')
Args(0)
Menu('Reverzní záznamy')
MenuOrder(25)
{
    my ( $self, $c ) = @_;
}

sub detail :
Chained('rr')
PathPart('')
Args(0)
Form
{
    my ( $self, $c ) = @_;
    my $rr = $c->stash->{rr};

    my $history_rrs = $rr->history_view(
        {},
        { order_by => { -desc => 'changed' }}
    );

    my @history;

    HISTORY:
    while ( my $history = $history_rrs->next ) {
        push @history, $history;
    }

    if ( scalar @history ) {
        $c->stash->{history} = \@history;
    }

    $c->stash->{no_wrapper} = 1;
}

sub set_filter_field :Chained('base') :PathPart('set_filter_field') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

    if ( length $args->{value} ) {
        $c->session->{ FILTER_NAME() }{ $args->{field} } = $args->{value};
    }
    else {
        delete $c->session->{ FILTER_NAME() }{ $args->{field} };
    }

    $c->response->body('OK');
}

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
__END__

