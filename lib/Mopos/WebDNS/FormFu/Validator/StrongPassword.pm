package Mopos::WebDNS::FormFu::Validator::StrongPassword;

use strict;
use utf8;

use base 'HTML::FormFu::Validator';
use Data::Password qw(:all);

$DICTIONARY = 5;
$GROUPS     = 2;
$MINLEN     = 6;
$MAXLEN     = 32;

sub validate_value {
    my $self = shift;
    my ( $value, $params ) = @_;

    my $rc = IsBadPassword($value) || return 1;

    die HTML::FormFu::Exception::Validator->new({
        message => "Příliš jednoduché heslo ($rc)",
    });

}

1;
