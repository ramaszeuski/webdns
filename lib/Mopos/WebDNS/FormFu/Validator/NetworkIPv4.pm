package Mopos::WebDNS::FormFu::Validator::NetworkIPv4;

use strict;
use utf8;

use Net::IP;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params ) = @_;
    my $c = $self->form->stash->{context};

    my $ip = new Net::IP ( $value, 4 ) or die HTML::FormFu::Exception::Validator->new({
        message => 'Neplatná síť IPv4' . $value,
    });

    if ( $ip->size() != 256 ) {
        die HTML::FormFu::Exception::Validator->new({
            message => 'Neplatná IPv4 síť třidy C',
        });
    }

    return 1;

}

1;


