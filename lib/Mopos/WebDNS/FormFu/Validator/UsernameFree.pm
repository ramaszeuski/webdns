package Mopos::WebDNS::FormFu::Validator::UsernameFree;

use strict;
use utf8;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params ) = @_;
    my $c = $self->form->stash->{context};

    return 1 if $c->stash->{user} && $c->stash->{user}->login eq $value;

    return 1 if $c->model('DB::User')->search(
        {
            login   => { ilike => $value },
            deleted => undef,
        }
    )->count == 0;

    die HTML::FormFu::Exception::Validator->new({
        message => 'Zadané uživatelské jméno je již použito',
    });
}

1;


