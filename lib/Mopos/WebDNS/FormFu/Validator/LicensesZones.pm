package Mopos::WebDNS::FormFu::Validator::LicensesZones;

use strict;
use utf8;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params) = @_;
    my $c = $self->form->stash->{context};

    return 1 if ! $params->{customer_id};
    return 1 if $c->stash->{zone};

    my $license = $c->model('DB::License')->search(
        {
            customer_id => $params->{customer_id},
            feature_id  => 'zones',
        }
    )->first;

    return 1 if $license && $license->free() > 0;

    die HTML::FormFu::Exception::Validator->new({
        message => 'Zákazník nema volné licence pro přidaní domény',
    });
}

1;


