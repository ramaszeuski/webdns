package Mopos::WebDNS::FormFu::Validator::ZonenameFree;

use strict;
use utf8;
use Net::IP;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params ) = @_;

    my $c = $self->form->stash->{context};

    if ( $value != $params->{name} ) {
        my $ip = Net::IP->new( $params->{network} );
        $value = $ip->reverse_ip();
        $value =~ s/\.$//;
    }

    my $exists_cname = $c->model('DB::RR')->search(
        {
            name    => lc( $value ),
            deleted => undef,
            type    => 'CNAME'
        }
    )->first;

    die HTML::FormFu::Exception::Validator->new({
        message => 'Zadaný nazev domény je již použit jako CNAME v doméně ' . $exists_cname->zone->name,
    }) if $exists_cname;

    return 1;

}

1;
