package Mopos::WebDNS::FormFu::Validator::SlaveZone;

use strict;
use utf8;
use Net::IP;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params ) = @_;

    return 1 if $value !~ /arpa$/;

    die HTML::FormFu::Exception::Validator->new({
        message => 'Sekundarní reverzní domény nejsou porporované',
    });
}

1;

