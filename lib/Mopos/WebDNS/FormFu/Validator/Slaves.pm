package Mopos::WebDNS::FormFu::Validator::Slaves;

use strict;
use utf8;

use Net::IP;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params ) = @_;
    my $c = $self->form->stash->{context};

    IP:
    foreach my $ip (split /[\s\;\,]+/, $value) {

        my $address = new Net::IP ( $ip ) or die HTML::FormFu::Exception::Validator->new({
            message => "Neplatná IP adresa $ip",
        });
    }

    return 1;

}

1;

