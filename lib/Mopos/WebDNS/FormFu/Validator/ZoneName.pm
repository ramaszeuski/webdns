package Mopos::WebDNS::FormFu::Validator::ZoneName;

use strict;
use utf8;

use Data::Validate::Domain qw(is_domain);

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params ) = @_;
    my $c = $self->form->stash->{context};

    my $value_idn = $c->model('DB')->schema->idn_encode( $value );

#    $c->log->debug( "$value -> $value_idn" );

    return 1 if is_domain( $value_idn );

    die HTML::FormFu::Exception::Validator->new({
        message => 'Neplatný název domény',
    });
}

1;


