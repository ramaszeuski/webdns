package Mopos::WebDNS::FormFu::Validator::NetworkIPv6;

use strict;
use utf8;

use Net::IP;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params ) = @_;
    my $c = $self->form->stash->{context};

    my $ip = new Net::IP ( $value, 6 );

    if ( ! $ip || $ip->prefixlen > 127 ) {
        die HTML::FormFu::Exception::Validator->new({
            message => 'Neplatná síť IPv6',
        });
    }

    return 1;

}

1;


