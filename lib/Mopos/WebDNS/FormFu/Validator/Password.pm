package Mopos::WebDNS::FormFu::Validator::Password;

use strict;
use utf8;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params ) = @_;
    my $c = $self->form->stash->{context};

    return 1 if  $value eq $c->user->password;

    die HTML::FormFu::Exception::Validator->new({
        message => 'Neplatné heslo',
    });

}

1;
