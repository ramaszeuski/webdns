package Mopos::WebDNS::FormFu::Validator::AllowedAXFR;

use strict;
use utf8;

use Net::DNS;
use Net::IP;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params ) = @_;
    my $c = $self->form->stash->{context};

    my @nameservers = split /[\s\,\;]+/, $value;

    NAMESERVER:
    foreach my $ip ( @nameservers ) {
        my $address = new Net::IP ( $ip ) or die HTML::FormFu::Exception::Validator->new({
            message => "Neplatná IP adresa $ip",
        });
    }

    my $resolver  = Net::DNS::Resolver->new;

    $resolver->nameservers( @nameservers );

    my $name = $c->stash->{zone} ? $c->stash->{zone}->name : $params->{name};

    $name = $c->model('DB')->schema->idn_encode( $name );

    my @zone = $resolver->axfr( $name );

    if ( scalar @zone ) {
        return 1;
    }

    die HTML::FormFu::Exception::Validator->new({
        message => 'Primarní servery neumožnují transfer domény',
    });
}

1;




