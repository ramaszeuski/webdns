use strict;
use warnings;

use Mopos::WebDNS;

BEGIN {
        $INC{'Mo/builder.pm'} = 1;
        $INC{'Mo/default.pm'} = 1;
}

my $app = Mopos::WebDNS->apply_default_middlewares(Mopos::WebDNS->psgi_app);
$app;

