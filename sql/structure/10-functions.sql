CREATE OR REPLACE FUNCTION rr_type_priority( "type" text ) RETURNS integer
    AS $$
begin
    case "type"
        when 'SOA'   then return 99;
        when 'NS'    then return 95;
        when 'MX'    then return 90;
        when 'A'     then return 85;
        when 'AAAA'  then return 80;
        when 'CNAME' then return 75;
        when 'SRV'   then return 70;        when 'TXT'   then return 65;
        else return 0;
    end case;
end;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION array_reverse(anyarray) RETURNS anyarray AS $$
SELECT ARRAY(
    SELECT $1[i]
    FROM generate_subscripts($1,1) AS s(i)
    ORDER BY i DESC
);
$$ LANGUAGE 'sql' STRICT IMMUTABLE;

CREATE OR REPLACE FUNCTION hostname_reverse( "hostname" text ) RETURNS text
    AS $$
begin
    return  array_reverse(string_to_array(hostname, '.'));
end;
$$ LANGUAGE plpgsql;

--    return  array_to_string(array_reverse(string_to_array(hostname, '.')),'.');
