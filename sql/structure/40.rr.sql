/*
begin;

create table "rr_bak" as select * from "rr";
drop table "rr" cascade;
*/


create table "rr" (
    "id"            integer not null default nextval('uid_seq'),
    "active"        bool not null default true,
    "state"         smallint not null default 0, -- 0 - OK, 2 - editace zakazana
    "changed"       timestamp(0) not null default now(),
    "deleted"       timestamp(0),
    "zone_id"       integer,
    "customer_id"   integer,

    "name"          text,
    "type"          varchar(16),
    "class"         varchar(16) default 'IN',
    "ttl"           integer,

    "preference"    integer, --MX, SRV
    "weight"        integer, --SRV
    "port"          integer, --SRV
    "address"       inet, --PTR
    "value"         text,

    "options"       text,
    "notes"         text,

    primary key ("id"),
    foreign key     ("zone_id")
        references "zone"("id") on update cascade on delete cascade
);
create index rr_type_idx on rr (type);
/*
insert into "rr" select
    "id",
    "active",
    "state",
    "changed",
    "deleted",
    "zone_id",
    "customer_id",

    "name",
    "type",       --begin;
    "class",
    "ttl",
null, null, null, "address", null, "content" from "rr_bak";
*/

begin;
drop view "rr_view";
create view "rr_view" as
select
    "rr".*,
    "zone"."type" as "zone_type",
    "zone"."deleted" as "zone_deleted",
    rr_type_priority("rr"."type") as "type_priority",
    "customer"."name" as "customer_name",
    text("rr"."address") as "address_text",
    hostname_reverse("rr"."name") as "name_reverse"
    from "rr"
    join "zone" on ("rr"."zone_id" = "zone"."id")
    left join "customer" on ("rr"."customer_id" = "customer"."id")
;
commit;

create table "rr_history" (
    "id"            integer not null default nextval('uid_seq'),
    "rr_id"         integer not null,

    "changed"       timestamp(0) not null default now(),
    "log_id"        integer,

    "ttl"           integer,
    "preference"    integer, --MX, SRV
    "weight"        integer, --SRV
    "port"          integer, --SRV
    "address"       inet, --PTR
    "value"         text,

    primary key ("id"),
    foreign key     ("rr_id")
        references "rr" ("id") on update cascade on delete cascade
);

create view "rr_history_view" as
select
    "rr_history".*,
    "log"."address" as "ip",
    "user"."login" as "user_login",
    "user"."firstname" as "user_firstname",
    "user"."lastname" as "user_lastname"
    from "rr_history"
    left join "log" on ("rr_history"."log_id" = "log"."id")
    left join "user" on ("log"."user_id" = "user"."id")
;

