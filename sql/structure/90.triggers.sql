CREATE OR REPLACE FUNCTION ins_zone() RETURNS "trigger"
    AS $$
begin
    update license set used = used + 1
    where customer_id=new.customer_id and feature_id = 'zones';
    return new;
end;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION del_zone() RETURNS "trigger"
    AS $$
begin
    if ( old.deleted is null )
    then
        update license set used = used - 1
        where customer_id=old.customer_id and feature_id = 'zones';
    end if;
    return old;
end;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION upd_zone() RETURNS "trigger"
    AS $$
begin

    if ( old.deleted is null and new.deleted is not null )
    then
        update license set used = used - 1
        where customer_id=old.customer_id and feature_id = 'zones';
    end if;

    if ( old.deleted is not null and new.deleted is null )
    then
        update license set used = used + 1
        where customer_id=old.customer_id and feature_id = 'zones';
    end if;

    if ( old.customer_id <> new.customer_id )
    then
        update license set used = used - 1
        where customer_id=old.customer_id and feature_id = 'zones';
        update license set used = used + 1
        where customer_id=new.customer_id and feature_id = 'zones';
    end if ;

    return new;
end;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tr_ins_zone AFTER INSERT
    ON zone FOR EACH ROW EXECUTE PROCEDURE ins_zone();
CREATE TRIGGER tr_del_zone AFTER DELETE
    ON zone FOR EACH ROW EXECUTE PROCEDURE del_zone();
CREATE TRIGGER tr_upd_zone AFTER UPDATE
    ON zone FOR EACH ROW EXECUTE PROCEDURE upd_zone();

CREATE OR REPLACE FUNCTION ins_user() RETURNS "trigger"
    AS $$
begin
    update license set used = used + 1
    where customer_id=new.customer_id and feature_id = 'users';
    return new;
end;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION del_user() RETURNS "trigger"
    AS $$
begin
    if ( old.deleted is null )
    then
        update license set used = used - 1
        where customer_id=old.customer_id and feature_id = 'users';
    end if;
    return old;
end;
$$ LANGUAGE plpgsql;

-- uzivatele

CREATE OR REPLACE FUNCTION upd_user() RETURNS "trigger"
    AS $$
begin

    if ( old.deleted is null and new.deleted is not null )
    then
        update license set used = used - 1
        where customer_id=old.customer_id and feature_id = 'users';
    end if;

    if ( old.deleted is not null and new.deleted is null )
    then
        update license set used = used + 1
        where customer_id=old.customer_id and feature_id = 'users';
    end if;

    if ( old.customer_id <> new.customer_id )
    then
        update license set used = used - 1
        where customer_id=old.customer_id and feature_id = 'users';
        update license set used = used + 1
        where customer_id=new.customer_id and feature_id = 'users';
    end if ;

    return new;
end;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tr_ins_user AFTER INSERT
    ON "user" FOR EACH ROW EXECUTE PROCEDURE ins_user();
CREATE TRIGGER tr_del_user AFTER DELETE
    ON "user" FOR EACH ROW EXECUTE PROCEDURE del_user();
CREATE TRIGGER tr_upd_user AFTER UPDATE
    ON "user" FOR EACH ROW EXECUTE PROCEDURE upd_user();

--- historie zmen rr


CREATE OR REPLACE FUNCTION upd_rr() RETURNS "trigger"
    AS $$
begin

    insert into "rr_history"
        ("rr_id", "ttl", "preference", "weight", "port", "address", "value")
    values
        (new.id, new.ttl, new.preference, new.weight, new.port, new.address, new.value);

    return new;
end;
$$ LANGUAGE plpgsql;

CREATE TRIGGER tr_upd_rr AFTER UPDATE
    ON "rr" FOR EACH ROW EXECUTE PROCEDURE upd_rr();
CREATE TRIGGER tr_add_rr AFTER INSERT
    ON "rr" FOR EACH ROW EXECUTE PROCEDURE upd_rr();
