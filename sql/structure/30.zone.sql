create table "zone" (
    "id"             integer not null default nextval('uid_seq'),
    "active"         bool not null default true,
    "state"          smallint not null default 0, -- 0 - OK, 1 - refreshing secondary... 10 - neni u nas
    "changed"        timestamp(0) not null default now(),
    "deleted"        timestamp(0),
    "customer_id"    integer,

    "type"           integer not null default 1, -- 1 master, 2 slave, 3 reverse
    "name"           text,

    "options"        text,

    "notes"          text,

    "dnssec"        bool not null default false,
    "masters"       text,
    "allow_transfer" text,
    "max_rr"        integer not null default 100000,
    "expire"        date,
    primary key ("id")
);

create table "whois" (
    "id"      integer not null default nextval('uid_seq'),
    "zone_id" integer not null,
    "updated" timestamp(0),
    "data"    text,
    primary key ("id"),
    foreign key     ("zone_id")
        references "zone"("id") on update cascade on delete cascade
);

begin;
drop view "zone_view";
create view "zone_view" as
select
    "zone".*,
    "customer"."name" as "customer_name",
    text("network") as "network_text",
    hostname_reverse("zone"."name") as "name_reverse"
    from "zone"
    left join "customer" on ("zone"."customer_id" = "customer"."id")
;
end;

