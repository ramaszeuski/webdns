create sequence "uid_seq" start 10000;

create table "subject" (
    "id"        integer not null default nextval('uid_seq'),
    "active"    bool not null default true,
    "created"   timestamp(0) not null default now(),
    "deleted"   timestamp(0),
    "login"     varchar(32),
    "password"  varchar(32),
--jmeno, prijmeni / nazev
    "name"      text,
    "firstname" text,
    "lastname"  text,
--kontakty
    "email"     text,
    "messenger" text,
    "phone"     text,
    "cellular"  text,
    "fax"       text,
--adresa
    "country"   text,
    "region"    text,
    "city"      text,
    "address"   text,
    "zip"       text,
    "profile"   text,
    "notes"     text
);

create table "customer" (
    "ic" varchar(16),
    "dic" varchar(16),
    "sync_key" text,
    "bank_account" text,
primary key   ("id")
) inherits ("subject");

create table "user" (
    "customer_id" integer,
    "roles" text,
    unique ("login"),
    primary key ("id")
) inherits ("subject");

create table "log" (
    "id"            integer not null default nextval('uid_seq'),
    "user_id"       integer,
    "timestamp"     timestamp(0) not null default current_timestamp,
    "event"         varchar(32) not null,
    "address"       varchar(15),
    "data"          text,
    primary key ("id")
);

create view "log_view" as
select
    "log".*,
    "user"."login" as "user_login",
    "user"."firstname" as "user_firstname",
    "user"."lastname" as "user_lastname",
    "customer"."id" as "customer_id",
    "customer"."name" as "customer_name"
    from "log"
    left join "user" on ("log"."user_id" = "user"."id")
    left join "customer" on ("user"."customer_id" = "customer"."id")
;

create view "user_view" as
select
    "user".*,
    "customer"."name" as "customer_name"
    from "user"
    left join "customer" on ("user"."customer_id" = "customer"."id")
;
