create table "license" (
    "id"            bigint not null default nextval('uid_seq'),
    "customer_id"    bigint not null,
    "feature_id"    varchar(32) not null,
    "granted"       integer not null default 0,
    "used"          integer default 0,
    primary key     ("id"),
    unique          ("customer_id", "feature_id"),
    foreign key     ("customer_id")
        references "customer"("id") on update cascade on delete cascade
);

