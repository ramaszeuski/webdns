use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Mopos::WebDNS';
use Mopos::WebDNS::Controller::Ranges;

ok( request('/ranges')->is_success, 'Request should succeed' );
done_testing();
