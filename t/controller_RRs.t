use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Mopos::WebDNS';
use Mopos::WebDNS::Controller::RRs;

ok( request('/rrs')->is_success, 'Request should succeed' );
done_testing();
