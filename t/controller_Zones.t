use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Mopos::WebDNS';
use Mopos::WebDNS::Controller::Zones;

ok( request('/zones')->is_success, 'Request should succeed' );
done_testing();
