use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Mopos::WebDNS';
use Mopos::WebDNS::Controller::DNSSEC;

ok( request('/dnssec')->is_success, 'Request should succeed' );
done_testing();
