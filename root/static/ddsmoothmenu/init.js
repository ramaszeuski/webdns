ddsmoothmenu.init(
    {
        mainmenuid:    "MainMenu", //menu DIV id
        orientation:   'h', //Horizontal or vertical menu: Set to "h" or "v"
        classname:     'ddsmoothmenu', //class added to menu's outer DIV
        contentsource: "markup", //"markup" or ["container_id", "path_to_menu_file"],
        customtheme: ["#595f81", '#373854'],
//        arrowimages: {down:['downarrowclass', '/ddsmoothmenu/down.gif', 23], right:['rightarrowclass', '/ddsmoothmenu/right.gif']},
    }
);

