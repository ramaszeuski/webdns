"oLanguage": {
    "sProcessing":   "<h3>Načítají se data</h4><img src='/img/loading-b2.gif' />",
    "sLengthMenu":   "Zobraz záznamů _MENU_",
    "sZeroRecords":  "Žádné záznamy nebyly nalezeny",
    "sEmptyTable":   "Tabulka neobsahuje žádné záznamy",
    "sInfo":         "Zobrazuji _START_ až _END_ z celkem _TOTAL_ záznamů",
    "sInfoEmpty":    "Zobrazuji 0 až 0 z 0 záznamů",
    "sInfoFiltered": "(filtrováno z celkem _MAX_ záznamů)",
    "sInfoPostFix":  "",
    "sSearch":       "Hledat:",
    "sUrl":          "",
    "oPaginate": {
       "sFirst":    "První",
       "sPrevious": "Předchozí",
       "sNext":     "Další",
       "sLast":     "Poslední"
    }
},
