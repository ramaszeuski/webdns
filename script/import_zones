#!/usr/bin/perl

use strict;
use warnings;
use utf8;

use FindBin qw($Bin);
use lib "$Bin/../lib";
use YAML;
use Getopt::Long::Descriptive;
use Net::DNS;
use Net::DNS::ZoneFile;
use Mopos::WebDNS::Schema;

use vars qw(
    $cfg
    $opt
    $usage
    $db
);

use constant OPTIONS    => [
  [ 'config=s',       'konfigurační soubor', { default => "$Bin/../mopos_webdns.yaml" } ],
  [ 'input|i=s',      'vstupni adresar', { required => 1 } ],
  [ 'use-filename', 'použit jmeno souboru jako jmeno zóny (v připade ze neni $ORIGIN)' ],
  [ 'customer|c:s',   'identifikator zakaznika', { required => 1 }],
  [ 'help|h',         'tato napověda' ],
];

our $VERSION = '0.1';

# parametry prikazove radky

( $opt, $usage ) = describe_options( '%c %o', @{ (OPTIONS) });

$usage->die() if $opt->help;

# konfiguracni soubor
for ( $opt->config ) {
    $cfg = YAML::LoadFile($_), last if $_ and -f $_;
}
die "Unable to open configuration file\n" unless $cfg;
die "Input directory does not exist\n" if ! -d $opt->input;

my $connect_info = $cfg->{'Model::DB'}{connect_info};

$connect_info = {
    %{ $connect_info },
    quote_char     => q("),
    name_sep       => q(.),
    pg_enable_utf8 => 1,
};

# database
$db = Mopos::WebDNS::Schema->connect( $connect_info );

opendir INPUT, $opt->input;

ZONE:
foreach my $filename ( readdir INPUT ) {

    my $file = $opt->input . '/' . $filename;

    next ZONE if ! -f $file;

    print "FILE: $filename ";

    import_zone( $file, $filename );


}

closedir INPUT;


sub import_zone {
    my $file     = shift;

    my $zonefile = new Net::DNS::ZoneFile(
        $file,  $opt->use_filename ? shift : ''
    ) or die "ERROR: $file\n";

    my $zone_name;

    my @rrs;

    eval { @rrs = $zonefile->read;  };

    if ( $@ ) {
        print "Chyba načtění zóny $@\n";
        return;
    }

    if ( ! scalar @rrs ) {
        print "Zóna nema žadný platný záznam\n";
        return;
    }

    RR:
    foreach my $rr ( @rrs ) {
        if ( $rr->type eq 'SOA' ) {
            $zone_name = $rr->name;
            last RR;
        }
    }

    if ( ! $zone_name ) {
        print "Neni možne zjistit jmeno zóny\n";
        return;
    }

    $zone_name = $db->idn_decode( $zone_name );

    my $rr_count = scalar @rrs;

    print "IMPORT ZONE: $zone_name RR: $rr_count";

    my $network = undef;
    my $type = 1;

    if ( $zone_name =~ /^(\d+)\.(\d+)\.(\d+)\.in-addr.arpa$/ ) {
        $network = "$3.$2.$1.0/24";
        $type = 3;
    }
    elsif ( $zone_name =~ /arpa$/) {
        print " UNSUPPORTED\n";
        return;
    }
#   $type = 4 if $zone_name =~ /ip6.arpa$/;

    if ( $network ) {
        print " NETWORK: $network";
    }

    my $zone = $db->resultset('Zone')->search(
        {
            name    => $zone_name,
            type    => $type,
            deleted => undef,
        }
    )->first;

    if ( $zone ) {
        print " UPDATE";
        $zone->rrs->delete();
    }
    else {
        $zone = $db->resultset('Zone')->create(
            {
                name        => $zone_name,
                type        => $type,
                network     => $network,
                customer_id => $opt->customer,
            }
        );
        print " CREATE";
    }
    print "\n";

    RR:
    foreach my $rr ( @rrs ) {
        my $rr_data = $db->resultset('RR')->parse( $rr->string );

        if ( ! $rr_data || $rr_data->{error} ) {
            print "Problem parsinga RR " . $rr_data->{error} // '' . "\n";
            next RR;
        }

        $rr_data->{name} = $db->idn_decode($rr->name);

        if ( $zone->type == 3
             && $rr->type eq 'PTR'
             && $rr->name =~ /^(\d+)\.(\d+)\.(\d+)\.(\d+)\.in-addr.arpa$/ ) {

            $rr_data->{address} = "$4.$3.$2.$1";
            $rr_data->{customer_id} = $opt->customer;
            $rr_data->{ttl} ||= 3600;
        }


        $zone->add_to_rrs( $rr_data );
    }


}
