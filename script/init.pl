#!/usr/bin/perl

use strict;
use warnings;
use utf8;

use FindBin qw($Bin);
use Template;
use File::Find;
use DBI;

my $tt = Template->new({
    ENCODING     => 'utf8',
    INCLUDE_PATH => "$Bin/../",
});

$tt->process(
    'root/tpl/config.yaml.tt2',
    \%ENV,
    $ENV{CATALYST_CONFIG},
) || die $tt->error->info();

my $dbh = DBI->connect(
    "dbi:Pg:dbname=$ENV{DB_NAME};host=$ENV{DB_HOST};port=$ENV{DB_PORT}",
    $ENV{DB_USER},
    $ENV{DB_PASSWORD},
    {AutoCommit => 1, RaiseError => 1, PrintError => 0},
);

my $sth = $dbh->table_info('', 'public', undef, undef);

if ( ! $sth->rows ) {
    find({
        wanted => \&do_sql,
        preprocess => sub { sort @_ },
    }, "$Bin/../sql"  );
}

$sth->finish;
$dbh->disconnect;

sub do_sql {
    /^\d\d/ || return;

    my $sql = '';

#    eval {
        $tt->process("sql/$_", \%ENV, \$sql );
        $dbh->do($sql);
#    };
}
